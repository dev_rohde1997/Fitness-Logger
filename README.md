# Fitness Logger

Fitness Logger is an android application which supports you during your workout.

## Dependencies

 - Gradle
 - MPAndroidChart-v3.0.1

## Usage

Please use *Android Studio* to edit or launch this application.

## Code

The main code base of the application can be found at (/app/src/main/java/rohde/fitnesslogger)[https://gitlab.com/dev_rohde1997/Fitness-Logger/-/tree/master/app/src/main/java/rohde/fitnesslogger]

## App Preview

A list of screenshots as an application preview

### Menu
<img src="res/screenshots/Homescreen.png"  width="270" height="555">

### Workout plan Overview
<img src="res/screenshots/Workoutplan_Overview.png"  width="270" height="555">

### Workout plan edit
<img src="res/screenshots/Workoutplan_Edit.png"  width="270" height="555">

### Workout tracking
<img src="res/screenshots/Workout_Tracking.png"  width="270" height="555">

### Statistic diagram
<img src="res/screenshots/Diagram.png"  width="270" height="555">

### Statistic list
<img src="res/screenshots/Results.png"  width="270" height="555">
