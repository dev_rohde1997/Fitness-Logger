package rohde.fitnesslogger.main;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Objects;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.new_exercise.New_Exercise_Activity;
import rohde.fitnesslogger.saving_classes.Constants;
import rohde.fitnesslogger.saving_classes.Training_Set;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;
import static rohde.fitnesslogger.saving_classes.Constants.SAVE_TAG_TEMP_WORKOUT_DATA;


public class HomeScreen_Fragment extends Fragment {

    FragmentManager fragmentManager;


    public HomeScreen_Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view =  inflater.inflate(R.layout.fragment_homescreen, container, false);
        fragmentManager = getChildFragmentManager();
        TextView tv_continue = view.findViewById(R.id.main_training_text_continue);
        TextView tv_new = view.findViewById(R.id.main_training_text_start);

        //IMAGES
        ImageView new_workout_img = view.findViewById(R.id.main_training_image);
        new_workout_img.setImageResource(R.drawable.start_workout_1);

        ImageView new_plan_img = view.findViewById(R.id.main_new_plan_image);
        new_plan_img.setImageResource(R.drawable.workout_plan_1);

        ImageView results_img = view.findViewById(R.id.main_results_image);

        ImageButton del = view.findViewById(R.id.main_del_data);
        del.setOnClickListener(v -> {
            SharedPreferences.Editor editor = getActivity().getSharedPreferences(Constants.SAVE_TAG_WORKOUT_PLAN, 0).edit();
            editor.clear().apply();
        });


        //NEW TRAINING
        RelativeLayout btn_enter_exercise = view.findViewById(R.id.main_training_btn);

        btn_enter_exercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Savings.recoverSavedData) {

                    new AlertDialog.Builder(getContext())
                            .setTitle(getString(R.string.new_workout_start))
                            .setMessage(getString(R.string.really_want_to_overwrite_data))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, (dialog, whichButton) -> {
                                SharedPreferences preferences_exercises = getActivity().getSharedPreferences(SAVE_TAG_TEMP_WORKOUT_DATA, 0);
                                preferences_exercises.edit().clear().apply();
                                Savings.recoverSavedData = false;
                                tv_continue.setVisibility(View.GONE);
                                new_workout_img.setForeground(getResources().getDrawable(R.drawable.shape_background_main));
                                tv_new.setText(getString(R.string.new_workout));
                                Intent intent = new Intent(getContext(), New_Exercise_Activity.class);
                                startActivity(intent);
                            })
                            .setNegativeButton(android.R.string.no, (dialog, whichButton)-> {
                            }).show();


                } else {
                    Intent intent = new Intent(getContext(), New_Exercise_Activity.class);
                    startActivity(intent);
                }

            }
        });

        RelativeLayout btn_open_results = view.findViewById(R.id.main_results_btn);
        btn_open_results.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Savings.currentItem = 0;
                Savings.changeItem = true;
            }
        });


        //NEW WORKOUT PLAN
        RelativeLayout btn_new_workout_plan = view.findViewById(R.id.main_new_plan_btn);
        btn_new_workout_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Savings.currentItem = 2;
                Savings.changeItem = true;
            }
        });

        //SHOW RECOVER DATA POP UP
        if (Savings.recoverSavedData) {
            Log.i(LOG_TAG, "New Recovery");

            //Show

            tv_continue.setVisibility(View.VISIBLE);
            tv_new.setText(getString(R.string.new_workout_start));
            new_workout_img.setForeground(getResources().getDrawable(R.drawable.shape_background_main_big));
            //pop_up_main_layout.setVisibility(View.VISIBLE);

            //ImageButton btn_recover_data = view.findViewById(R.id.main_recover_button_yes);
            tv_continue.setOnClickListener(v -> {
                Intent intent = new Intent(getContext(), New_Exercise_Activity.class);
                startActivity(intent);
                tv_continue.setVisibility(View.GONE);
                new_workout_img.setForeground(getResources().getDrawable(R.drawable.shape_background_main));
                tv_new.setText(getString(R.string.new_workout));
            });
        }
        return view;
    }

    @Override
    public void onResume() {
        if (!Savings.workout_added_sets.isEmpty()) {

            ArrayList<Training_Set> recover_training_set = new ArrayList<>(Savings.workout_added_sets);
            Savings.workout_added_sets.clear();
            Snackbar.make(Objects.requireNonNull(getView()), R.string.unsaved_workout, Snackbar.LENGTH_LONG)
                    .setAction(R.string.recover_exercise, v -> {
                        Savings.recoverSavedData = true;
                        Savings.workout_added_sets = new ArrayList<>(recover_training_set);
                        Intent intent = new Intent(getContext(), New_Exercise_Activity.class);
                        startActivity(intent);
                    })
                    .show();

        }
        super.onResume();
    }
}
