package rohde.fitnesslogger.main;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Map;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.saving_classes.Category;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Muscles;
import rohde.fitnesslogger.saving_classes.Training_Set;
import rohde.fitnesslogger.saving_classes.Workout;
import rohde.fitnesslogger.saving_classes.Workout_plan;
import rohde.fitnesslogger.util.GenerateDoubleArrayWeightSteps;
import rohde.fitnesslogger.util.FixedSpeedScroller;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;
import static rohde.fitnesslogger.saving_classes.Constants.SAVE_TAG_EXERCISER_DEFAULT_WEIGHT;
import static rohde.fitnesslogger.saving_classes.Constants.SAVE_TAG_TEMP_WORKOUT_DATA;
import static rohde.fitnesslogger.saving_classes.Constants.SAVE_TAG_TRAINING;
import static rohde.fitnesslogger.saving_classes.Constants.SAVE_TAG_WORKOUT_PLAN;
import static rohde.fitnesslogger.saving_classes.Constants.TAG_EXERCISE_NOT_SAVED;
import static rohde.fitnesslogger.saving_classes.Constants.TAG_WORKOUT_NOT_SAVED;

public class MainActivity extends FragmentActivity {

    ViewPager viewPager;
    FrameLayout main_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            new Thread(() -> {

                try {
                    initExerciser();
                } catch (Exception e) {
                    Log.i(LOG_TAG, "Error during initExerciser");
                    e.printStackTrace();
                }
                //Save loaded workouts
                loadSavedWorkouts();
                loadWorkoutPlan();
                loadUnsavedWorkoutData();
                initExerciseCategories();


            }).start();


        } catch (Exception e) {
            Toast.makeText(this, R.string.error_failed_loading_data, Toast.LENGTH_LONG).show();
            Log.e(getClass().getSimpleName() + LOG_TAG, e.getLocalizedMessage());
        }

        createViewPager();

    }

    private void createViewPager() {
        viewPager = findViewById(R.id.main_view_pager);
        MainPageSlideAdapter mainPageSlideAdapter = new MainPageSlideAdapter( getSupportFragmentManager());
        viewPager.setAdapter(mainPageSlideAdapter);
        viewPager.setCurrentItem(1);

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            Interpolator sInterpolator = new DecelerateInterpolator();
            FixedSpeedScroller scroller = new FixedSpeedScroller(viewPager.getContext(), sInterpolator);
            mScroller.set(viewPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }


        @SuppressLint("HandlerLeak") Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                viewPager.setCurrentItem(Savings.currentItem);
            }
        };


        new Thread(() -> {
            while (true) {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (Savings.changeItem) {
                    Savings.changeItem = false;
                    handler.sendEmptyMessage(0);
                }

            }
        }).start();
    }

    private void initExerciser()  throws IOException {


        InputStream inputStream = getResources().openRawResource(R.raw.data);
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(inputStream, StandardCharsets.UTF_8)
        );

        String line;
        reader.readLine(); //SKIP first line
        while ((line = reader.readLine()) != null) {
            String[] data = line.split(",");
            String name = getStringResourceByName(data[0]);
            int image = getImageResourceByName(data[1]);
            String step = data[2];
            double minValue = Double.parseDouble(data[3]);
            double maxValue = Double.parseDouble(data[4]);
            double default_value = Double.parseDouble(data[5]);
            String type = data[6];
            int size = data.length - 7;



            if (size < 0) size = 0;
            String[] muscles_ids = new String[size];
            System.arraycopy(data, 7, muscles_ids, 0, data.length - 7);


            //Get language muscle names
            String[] muscle_names = new String[muscles_ids.length];
            for (int i = 0; i < muscles_ids.length; i++) {
                //Get name from id
                String muscle_string = getStringResourceByName(muscles_ids[i]);
                //Add to global muscle list
                if (!Savings.muscles.contains(muscle_string)) {
                    Savings.muscles.add(muscle_string);
                    Muscles.front_muscles_with_resId.put(muscle_string, getImageResourceByName("body_model_front_"+muscles_ids[i]));
                    Muscles.back_muscles_with_resId.put(muscle_string, getImageResourceByName("body_model_back_"+muscles_ids[i]));
                }

                //Save name in new list
                muscle_names[i] = muscle_string;
            }
            Muscles.removeZeroIds();

            //Insert muscles_ids
            for (String muscle : muscles_ids) {
                if (!Savings.muscles.contains(muscle))
                    Savings.muscles.add(muscle);
            }

            Exerciser e = addExerciser(name, image, muscle_names, step, minValue, maxValue, default_value, type);


            for (String muscle : muscles_ids) {
                switch (muscle) {
                    case "hamstrings":
                        Savings.ex_hamstrings.put(e.getName(), e);
                        break;
                    case "quads":
                        Savings.ex_quads.put(e.getName(), e);
                        break;
                    case "glutes":
                        Savings.ex_glutes.put(e.getName(), e);
                        break;
                    case "calves":
                        Savings.ex_calves.put(e.getName(), e);
                        break;
                    case "triceps":
                    case "biceps":
                    case "lower_arm":
                        Savings.ex_arms.put(e.getName(), e);
                        break;
                    case "delta_front":
                    case "delta_back":
                        Savings.ex_shoulders.put(e.getName(), e);
                        break;
                    case "abs":
                    case "abs_outer":
                        Savings.ex_abs.put(e.getName(), e);
                        break;
                    case "chest":
                        Savings.ex_chest.put(e.getName(), e);
                        break;
                    case "latissimus":
                    case "teres":
                    case "trapcius":
                    case "lower_chest":
                        Savings.ex_back.put(e.getName(), e);
                        break;
                    default:
                        //TODO: Check
//                        Log.i(LOG_TAG, "Forgotten muscle in category: " + muscle);
                }
            }


//
//
//
//            switch (type) {
//                case "Endurance":
//                    Savings.ex_endurance.add(e.getName());
//                    break;
//                case "Strength":
//                    Savings.ex_strength.add(e.getName());
//                    break;
//            }
        }
        Collections.sort(Savings.exercisers_global);


        //Check saved default weights
        SharedPreferences sharedPreferences = getSharedPreferences(SAVE_TAG_EXERCISER_DEFAULT_WEIGHT, 0);

        Map map = sharedPreferences.getAll();
        for (Object key : map.keySet()) {
            Object data = map.get(key);
            if (data == null) {
                Toast.makeText(this, R.string.error_failed_loading_data, Toast.LENGTH_SHORT).show();
                break;
            }

            for (int i = 0; i < Savings.exercisers_global.size(); i++) {
                Exerciser e = Savings.exercisers_global.get(i);
                if (e.getName().equals(key.toString())) {
                    e.setDefaultWeight(Double.parseDouble(data.toString()));
                    break;
                }
            }

        }

    }

    private String getStringResourceByName(String aString) {
        String packageName = getPackageName();
        int resId = getResources().getIdentifier(aString, "string", packageName);
        if (resId == 0) {
            Log.i(LOG_TAG, String.format("Unable to find String resource for %s", aString));
            return aString;
        }
        return getString(resId);
    }

    private int getImageResourceByName(String aString) {
        String packageName = getPackageName();
        int resID = getResources().getIdentifier(aString, "drawable", packageName);
//        if (resID == 0)
//            Log.i(LOG_TAG, String.format("Unable to find id for %s", aString));
        return resID;
    }

    private Exerciser addExerciser(String name, int image, String[] muscleGroup, String stepSizeString, double minValue, double maxValue, double default_value, String type) {
        if (image == 0) image = R.drawable.questionmark;

        String[] stepSizeSplit = stepSizeString.split(";");
        Double[] steps = null;

        if (stepSizeSplit.length == 1) {
            //Generate Steps with given step size, min and max
            double stepSize = Double.parseDouble(stepSizeString);
            steps = GenerateDoubleArrayWeightSteps.generateArray(stepSize, minValue, maxValue);
        } else {
            //Use given list
            steps = GenerateDoubleArrayWeightSteps.generateArray(stepSizeSplit);
        }

        Exerciser e = new Exerciser(name, image, muscleGroup, steps, default_value, type);
        Savings.exercisers_global.add(e);
        return e;
    }

    //TODO: Add image res
    private void initExerciseCategories() {
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.chest), R.drawable.questionmark, Savings.ex_chest));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.back), R.drawable.questionmark, Savings.ex_back));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.shoulders), R.drawable.questionmark, Savings.ex_shoulders));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.arms), R.drawable.questionmark, Savings.ex_arms));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.abs), R.drawable.questionmark, Savings.ex_abs));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.quads), R.drawable.questionmark, Savings.ex_quads));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.calves), R.drawable.questionmark, Savings.ex_calves));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.glutes), R.drawable.questionmark, Savings.ex_glutes));
        Savings.exercise_muscles_categories.add(new Category(getString(R.string.hamstrings), R.drawable.questionmark, Savings.ex_hamstrings));
//        Collections.sort(Savings.exercise_muscles_categories);
    }

    private void loadSavedWorkouts() {
        SharedPreferences settings = getSharedPreferences(SAVE_TAG_TRAINING, 0);
        Map map = settings.getAll();
        for (Object key : map.keySet()) {
            Object data = map.get(key);
            if (data == null) {
                Toast.makeText(this, R.string.error_failed_loading_data, Toast.LENGTH_SHORT).show();
                break;
            }
            //Log.i(getClass().getSimpleName() + LOG_TAG, "Loaded Data: " + key.toString());
            Workout w = new Workout(key.toString(), data.toString());
            Savings.workouts.add(w);
        }
    }

    private void loadWorkoutPlan() {
        SharedPreferences preferences_exercises = getSharedPreferences(SAVE_TAG_WORKOUT_PLAN, 0);
        Map exercise_map = preferences_exercises.getAll();
        for (Object key : exercise_map.keySet()) {
            Object data = exercise_map.get(key);
            if (data == null) {
                Toast.makeText(this, R.string.error_failed_loading_data, Toast.LENGTH_SHORT).show();
                break;
            }
            Savings.workout_plan.add(new Workout_plan(Long.parseLong(key.toString()), data.toString()));
        }
    }

    private void loadUnsavedWorkoutData() {
        SharedPreferences preferences_exercises = getSharedPreferences(SAVE_TAG_TEMP_WORKOUT_DATA, 0);
        String unsaved_workout_data = preferences_exercises.getString(TAG_WORKOUT_NOT_SAVED, null);
        String unsaved_training_set_data = preferences_exercises.getString(TAG_EXERCISE_NOT_SAVED, null);

        if (unsaved_training_set_data != null && !unsaved_training_set_data.equals("[]")) {
            Savings.recoverSavedData = true;
        }

        if (unsaved_workout_data != null && !unsaved_workout_data.equals("[]"))
            Savings.recoverSavedData = true;
    }



    private void clearStorage(String name) {
        getApplicationContext().getSharedPreferences(name, 0).edit().clear().apply();
    }

    private void printWorkout() {
        for (Workout w : Savings.workouts) {
            Log.i(getClass().getSimpleName() + LOG_TAG, "Workout: " + w.getId());
            for (Training_Set training_set : w.getComplete_trainingSet()) {
                for (Exercise exercise : training_set.getExercises()) {
                    Log.i(getClass().getSimpleName() + LOG_TAG, exercise.toString());
                }
            }
        }
    }

    private void printWorkoutPlan() {
        for (Workout_plan w : Savings.workout_plan) {
            Log.i(getClass().getSimpleName() + LOG_TAG, "Saved Workout Plan: " + w.getId() + ", " + w.getName() + w.getExerciser());
            if (w.getExerciser() == null) continue;
            for (Exerciser e : w.getExerciser()) {
                Log.i(getClass().getSimpleName() + LOG_TAG, e.toString());
            }

        }
    }

    private ArrayList<String> removeDuplicates(ArrayList<String> array) {
        LinkedHashSet<String> hashSet = new LinkedHashSet<>(array);
        return new ArrayList<>(hashSet);
    }

    @Override
    public void onBackPressed() {
        viewPager.setCurrentItem(1);
    }
}


