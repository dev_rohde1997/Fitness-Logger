package rohde.fitnesslogger.main;

import android.annotation.SuppressLint;
import android.view.View;

import java.util.ArrayList;
import java.util.HashMap;

import rohde.fitnesslogger.saving_classes.Category;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Training_Set;
import rohde.fitnesslogger.saving_classes.Workout;
import rohde.fitnesslogger.saving_classes.Workout_plan;

public class Savings {
    //Workouts
    public static ArrayList<Workout> workouts = new ArrayList<>();

    //Workout
    public static ArrayList<Training_Set> workout_added_sets = new ArrayList<>();
    public static ArrayList<Exercise> workout_sets_to_add = new ArrayList<>();
    public static long current_workout_plan_id = -1;

    //Training Plan
    public static ArrayList<Workout_plan> workout_plan = new ArrayList<>();
    public static ArrayList<String> muscles = new ArrayList<>();

    //Exerciser
    public static ArrayList<Exerciser> exercisers_global = new ArrayList<>();

    //Handler booleans
    public static boolean refresh_selected_list = false;
    public static boolean changeAddExList = false;


    //Workout plan
    public static ArrayList<Exerciser> workout_plan_exercisers_in_plan = new ArrayList<>();
    public static ArrayList<Exerciser> workout_plan_new_exerciser_to_add = new ArrayList<>();
    public static String current_workout_plan_name = null;
    public static boolean workout_plan_content_to_add_changed = false;
    public static boolean workout_plan_exerciser_to_display_changed = false;

    //Muscles Variables
    public static ArrayList<Category> exercise_muscles_categories = new ArrayList<>();

    //Exercise Category Lists - NEW
    //TODO: Statt lower back in den Bildern gluteus
    public static HashMap<String, Exerciser> ex_abs = new HashMap<>();
    public static HashMap<String, Exerciser> ex_chest = new HashMap<>();
    public static HashMap<String, Exerciser> ex_back = new HashMap<>();
    public static HashMap<String, Exerciser> ex_quads = new HashMap<>();
    public static HashMap<String, Exerciser> ex_glutes = new HashMap<>();
    public static HashMap<String, Exerciser> ex_hamstrings = new HashMap<>();
    public static HashMap<String, Exerciser> ex_calves = new HashMap<>();
    public static HashMap<String, Exerciser> ex_arms = new HashMap<>();
    public static HashMap<String, Exerciser> ex_shoulders = new HashMap<>();


    //AB Training
    public static String chosen_ab_training_category = null;
    public static String chosen_ab_training_exercise = null;
    public static String current_exerciser = null;
    public static boolean ab_training_category_chosen = false;
    public static boolean ab_training_is_active = false;

    @SuppressLint("StaticFieldLeak")
    public static View scrollItem = null;

    //View Pager
    public static boolean changeItem = false;
    public static int currentItem = 1;

    //Recover data
    public static boolean recoverSavedData = false;
    public static boolean saved_ab_training_data = false;

    //Filter Muscle Chosen Muscle Category
    public static HashMap<String, Exerciser> chosen_muscle_category;
    public static boolean chosen_muscle_category_changed = false;





}
