package rohde.fitnesslogger.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.new_exercise.New_Exercise_Menu;
import rohde.fitnesslogger.new_workout_plan.New_Workout_Plan_Menu;
import rohde.fitnesslogger.new_workout_plan.New_Workout_Plan_Overview;
import rohde.fitnesslogger.results.Result_Exercise_List;


public class MainPageSlideAdapter extends FragmentStatePagerAdapter {



    public MainPageSlideAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: return new Result_Exercise_List();
            case 1: return new HomeScreen_Fragment();
            case 2: return new New_Workout_Plan_Menu();
            default: return null;
        }
    }

}
