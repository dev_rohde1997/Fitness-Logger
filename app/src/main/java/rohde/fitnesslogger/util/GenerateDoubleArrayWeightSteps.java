package rohde.fitnesslogger.util;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

public class GenerateDoubleArrayWeightSteps {

    public static Double[] generateArray(double stepSize, double minValue, double maxValue) {

        ArrayList<Double> arrayList = new ArrayList<>();
        for (double d = minValue; d <= maxValue; d += stepSize ) {
            arrayList.add(d);
            if (minValue == maxValue) break;
        }
        Double[] list = new Double[arrayList.size()];
        return arrayList.toArray(list);
    }

    public static Double[] generateArray(String[] steps) {

        ArrayList<Double> arrayList = new ArrayList<>();

        for (String s : steps) {
            double d = Double.parseDouble(s);
            arrayList.add(d);
        }

        Double[] list = new Double[arrayList.size()];
        return arrayList.toArray(list);
    }
}
