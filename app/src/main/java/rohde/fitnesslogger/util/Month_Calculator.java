package rohde.fitnesslogger.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Month_Calculator {

    public static int month_to_int(String s) {
        switch (s) {
            case "Jan": return 0;
            case "Feb": return 1;
            case "Mar": return 2;
            case "Apr": return 3;
            case "May": return 4;
            case "Jun": return 5;
            case "Jul": return 6;
            case "Aug": return 7;
            case "Sep": return 8;
            case "Oct": return 9;
            case "Nov": return 10;
            case "Dec": return 11;
            default: return -1;
        }
    }

    public static Date getFirstDayOfWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        Calendar first_day_of_week = Calendar.getInstance(new Locale("de","DE"));
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SUNDAY:
                first_day_of_week.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) - 6);
                return first_day_of_week.getTime();
            case Calendar.MONDAY:
                return date;
            default:
                first_day_of_week.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) - c.get(Calendar.DAY_OF_WEEK) + 2);
                return first_day_of_week.getTime();
        }

    }

    public static Date getLastDayOfWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        Calendar last_day_of_week = Calendar.getInstance(new Locale("de","DE"));
        switch (c.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.SUNDAY:
                return date;
            case Calendar.MONDAY:
                last_day_of_week.set(Calendar.DAY_OF_YEAR, c.get(Calendar.DAY_OF_YEAR) + 6);
                return last_day_of_week.getTime();
            default:
                Date firstDay = Month_Calculator.getFirstDayOfWeek(date);
                Calendar c2 = Calendar.getInstance();
                c2.setTime(firstDay);
                last_day_of_week.set(Calendar.DAY_OF_YEAR, c2.get(Calendar.DAY_OF_YEAR) + 6);
                return last_day_of_week.getTime();

        }
    }
}
