package rohde.fitnesslogger.util;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import rohde.fitnesslogger.new_exercise.New_Filled_Exercise_Element;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

public class ExerciseLogCreator {
    public static LinearLayout createExerciseLog(Context context, FragmentManager fragmentManager, int repeats, double weight, int pos) throws IllegalArgumentException{

        // CREATE LINEAR LAYOUT
        final LinearLayout LL_log_exercise = new LinearLayout(context);
        LL_log_exercise.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        LL_log_exercise.setOrientation(LinearLayout.VERTICAL);
        LL_log_exercise.setId(View.generateViewId());

        New_Filled_Exercise_Element new_filled_exercise_element =
                New_Filled_Exercise_Element.newInstance(repeats, weight, pos);

        //ADD Fragment
        //TODO: BUG -> java.lang.ClassCastException: android.widget.RelativeLayout$LayoutParams cannot be cast to android.widget.LinearLayout$LayoutParam - WOOO???

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(LL_log_exercise.getId(), new_filled_exercise_element)
                .commit();
        return LL_log_exercise;
    }
}
