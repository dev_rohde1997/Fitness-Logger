package rohde.fitnesslogger.new_exercise;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import rohde.fitnesslogger.R;


public class New_Exerciser_Element extends Fragment {

    private static final String LABEL_TXT = "text";
    private static final String IMAGE_SRC = "src";
    private static final String ID = "id";

    private String label_text;
    private int image_src;
    private long id;

    public static New_Exerciser_Element newInstance(String text, int image, long id) {
        New_Exerciser_Element fragment = new New_Exerciser_Element();
        Bundle args = new Bundle();
        args.putString(LABEL_TXT, text );
        args.putInt(IMAGE_SRC, image);
        args.putLong(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            label_text = getArguments().getString(LABEL_TXT);
            image_src = getArguments().getInt(IMAGE_SRC);
            id = getArguments().getLong(ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_exersicer_element, container, false);
        final ImageView image = view.findViewById(R.id.new_ex_element_image_view);
        final TextView headline = view.findViewById(R.id.new_ex_element_headline);
        RelativeLayout relativeLayout = view.findViewById(R.id.new_ex_element_layout);

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .replace(R.id.new_exercise_activity_layout,
                                New_Exercise_Enter.newInstance(label_text, id))
                        .commit();
            }
        });


        headline.setText(label_text);
        image.setImageResource(image_src);

        return view;
    }
}
