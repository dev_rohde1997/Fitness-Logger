package rohde.fitnesslogger.new_exercise;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.TextViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.Collections;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Workout_plan;

import static rohde.fitnesslogger.R.dimen;
import static rohde.fitnesslogger.R.drawable;
import static rohde.fitnesslogger.R.id;
import static rohde.fitnesslogger.R.layout;


public class New_Exercise_Menu extends Fragment {

    Button btn_continue_no_plan;
    ImageButton btn_back;
    public New_Exercise_Menu() {
        // Required empty public constructor
    }

    public static New_Exercise_Menu newInstance() {
        return new New_Exercise_Menu();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(layout.fragment_new_exercise_menu, container, false);

        //Init
        ScrollView new_ex_menu_scroll = view.findViewById(id.new_ex_menu_scroll);
        LinearLayout no_plan_layout = view.findViewById(id.new_ex_menu_no_workout_plan_layout);
        btn_continue_no_plan = view.findViewById(id.new_ex_menu_continue);
        btn_back = view.findViewById(id.new_ex_menu_back_btn);
        LinearLayout chose_plan_layout = view.findViewById(id.new_ex_menu_chose_plan_layout);

        Button create_training_btn = view.findViewById(R.id.new_ex_menu_no_workout_plan_new_workout_plan);
        create_training_btn.setOnClickListener(v -> {
            Savings.currentItem = 2;
            Savings.changeItem = true;
            getActivity().finish();
        });


        //OnClickListener
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        if (Savings.workout_plan.size() == 0) {
            no_plan_layout.setVisibility(View.VISIBLE);
            new_ex_menu_scroll.setVisibility(View.GONE);
            //TODO: Wieder rein?
            /*RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.topMargin = toPixel(10);
            params.leftMargin = toPixel(20);
            params.rightMargin = toPixel(20);
            params.addRule(RelativeLayout.BELOW, no_plan_layout.getId());
            btn_continue_no_plan.setLayoutParams(params);*/
        } else {
            no_plan_layout.setVisibility(View.GONE);
            new_ex_menu_scroll.setVisibility(View.VISIBLE);


            //For each training plan
            Collections.sort(Savings.workout_plan);
            for (Workout_plan w : Savings.workout_plan) {
                chose_plan_layout.addView(createButtonStartTrainingByPlan(w));
            }
        }

        btn_continue_no_plan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .replace(id.new_exercise_activity_layout, New_Exercise_By_Plan.newInstance(-1))
                        .commit();
            }
        });

        return view;
    }

    private int toPixel(int dps) {
        final float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }


    private Button createButtonStartTrainingByPlan(final Workout_plan wp) {
        Button b = new Button(getContext());
        b.setText(String.valueOf(wp.getName()));
        b.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        b.setBackgroundResource(drawable.shape_round_button_white_trans);
        //Params
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.bottomMargin = getResources().getDimensionPixelSize(dimen.avg_margin);
        params.leftMargin = getResources().getDimensionPixelSize(dimen.avg_margin);
        params.rightMargin = getResources().getDimensionPixelSize(dimen.avg_margin);
        params.topMargin = getResources().getDimensionPixelSize(dimen.avg_margin);
        params.height = toPixel(100);

        b.setLayoutParams(params);
        b.setPadding(20, 10, 20, 10);
        b.setTextColor(Color.WHITE);
        b.setAllCaps(false);
        b.setBackgroundResource(R.drawable.shape_round_corner_buttons_light);
        b.setAutoSizeTextTypeUniformWithConfiguration(
                getResources().getDimensionPixelSize(dimen.text_size_small),
                getResources().getDimensionPixelSize(dimen.text_size_default),
                getResources().getDimensionPixelSize(dimen.auto_text_size_granularity),
                TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);

        b.setOnClickListener(v -> {
            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction
                    .add(id.new_exercise_activity_layout, New_Exercise_Start.newInstance(wp.getId()))
                    .commit();
        });
        return b;
    }
}
