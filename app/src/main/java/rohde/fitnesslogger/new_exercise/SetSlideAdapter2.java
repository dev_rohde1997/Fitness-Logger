package rohde.fitnesslogger.new_exercise;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import rohde.fitnesslogger.main.Savings;


public class SetSlideAdapter2 extends FragmentStatePagerAdapter {

    String text = "";

    public SetSlideAdapter2(FragmentManager fm, String text) {
        super(fm);
        this.text = text;
    }



    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: return New_Exercise_Enter_Sets.newInstance(text);
            case 1: return new New_Exercise_Enter_Choose_AB();
            default: return null;
        }
    }
}