package rohde.fitnesslogger.new_exercise;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.util.ExerciseLogCreator;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link New_Exercise_Enter_Sets#newInstance} factory method to
 * create an instance of this fragment.
 */
public class New_Exercise_Enter_Sets extends Fragment {

    //VARS
    private static final String NAME = "name";
    private String name;


    // UI - VARS
    TextView tv_headline;
    LinearLayout log_layout;

    public New_Exercise_Enter_Sets() {
        // Required empty public constructor
    }


    public static New_Exercise_Enter_Sets newInstance(String name) {
        New_Exercise_Enter_Sets fragment = new New_Exercise_Enter_Sets();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_exercise_enter_sets, container, false);

        tv_headline = view.findViewById(R.id.enter_ex_headline);
        tv_headline.setText(name);

        log_layout = view.findViewById(R.id.enter_ex_log_ex_layout);

        int i = 0;
        for (Exercise e : Savings.workout_sets_to_add) {
            if (!e.getName().equals(name))
                continue;
            log_layout.addView(ExerciseLogCreator.createExerciseLog(getContext(), getFragmentManager(), e.getRepeats(), e.getWeight(), i));
            i++;
        }

        return view;
    }
}