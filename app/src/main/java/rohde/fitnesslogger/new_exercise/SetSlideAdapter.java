package rohde.fitnesslogger.new_exercise;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import rohde.fitnesslogger.main.HomeScreen_Fragment;
import rohde.fitnesslogger.new_workout_plan.New_Workout_Plan_Menu;
import rohde.fitnesslogger.results.Result_Exercise_List;

public class SetSlideAdapter extends FragmentStatePagerAdapter {

    String text = "";

    public SetSlideAdapter(FragmentManager fm, String text) {
        super(fm);
        this.text = text;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Fragment getItem(int i) {
        return New_Exercise_Enter_Sets.newInstance(text);
    }
}