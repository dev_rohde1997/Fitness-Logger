package rohde.fitnesslogger.new_exercise;

public interface IOnBackPressed {
    boolean onBackPressed();
}
