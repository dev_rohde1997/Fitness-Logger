package rohde.fitnesslogger.new_exercise;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.util.FixedSpeedScroller;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Constants;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Training_Set;

import static rohde.fitnesslogger.saving_classes.Constants.BUTTONS_HOLD_TIME;
import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;
import static rohde.fitnesslogger.saving_classes.Constants.MAX_REPEATS;
import static rohde.fitnesslogger.saving_classes.Constants.SAVE_TAG_SELF_CREATED_EXERCISER;

//TODO: Leicht auf die Buttons tippen und es passiert nichts -> Zusötzlich zum OnTouch noch OnCLick einführen

public class New_Exercise_Enter extends Fragment {
    //CONST
    private static final String LABEL_TXT = "text";
    private static final String ID = "id";

    //VARS
    private String label_text;
    private long id;

    boolean reps_min_pressed = false;
    boolean reps_plus_pressed = false;
    boolean weight_min_pressed = false;
    boolean weight_plus_pressed = false;

    // UI components
    TextView tv_current_repeats;
    TextView tv_current_weight;
    int current_repeats;
    double current_weight;
    TextView tv_error;
    TextView tv_ab_training;
    LinearLayout error_field_layout;
    ViewPager set_viewPager = null;

    Exerciser exerciser;
    Fragment fragment = this;

    Thread t1;

    boolean is_ab_training = false;

    //Static constructors
    public static New_Exercise_Enter newInstance(String txt, long id) {
        New_Exercise_Enter fragment = new New_Exercise_Enter();
        Bundle args = new Bundle();
        args.putString(LABEL_TXT, txt);
        args.putLong(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            label_text = getArguments().getString(LABEL_TXT);
            id = getArguments().getLong(ID);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_exercise_enter, container, false);

        exerciser = findExerciserByName(label_text);

        Savings.current_exerciser = label_text;

        //Set headline text and edit text

        //AB Training
        createViewPager(view);

        //Weight Buttons
        createWeightButtonPlus(view);
        createWeightButtonMinus(view);

        //Repeats Buttons
        createRepeatsButtonPlus(view);
        createRepeatsButtonMinus(view);

        //Back Button
        createBackButton(view);
        createInsertButton(view);

        // COMPLETE BUTTON
        createCompleteButton(view);

        //CHANGE VALUE THREAD AND HANDLER
        handleValueChange();



        //ADD Existing set values
        //createExistingEntries(view);





        return view;
    }

    private void createViewPager(View view) {

        //Check if ab must be recovered
        String saved_name = null;
        for (Exercise e : Savings.workout_sets_to_add) {
            if (saved_name == null) {
                saved_name = e.getName();
                continue;
            }
            if (!saved_name.equals(e.getName())) {
                is_ab_training = true;
                createViewPagerForAbTrainingRecovered(view);
                break;
            }
        }

        if (!is_ab_training) {
            createViewPagerOneFrag(view);
        }

        tv_ab_training = view.findViewById(R.id.enter_ex_AB);
        tv_ab_training.setOnClickListener(v -> {
            if (!is_ab_training) {
                createViewPagerForAbTraining(view);
                tv_ab_training.setForeground(getResources().getDrawable(R.drawable.icon_cancel_white));
                is_ab_training = true;
            } else {
                //Disable?
                tv_ab_training.setForeground(null);
                createViewPagerOneFrag(view);
                is_ab_training = false;
            }


            new Thread(() -> {
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                set_viewPager.setCurrentItem(1);
            }).start();
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createViewPagerForAbTraining(View view) {
        set_viewPager = view.findViewById(R.id.enter_ex_set_view_pager);
        SetSlideAdapter2 slideAdapter = new SetSlideAdapter2(getChildFragmentManager(), label_text);
        set_viewPager.setAdapter(slideAdapter);
        try {
            Field mScroller2;
            mScroller2 = ViewPager.class.getDeclaredField("mScroller");
            mScroller2.setAccessible(true);
            Interpolator sInterpolator = new DecelerateInterpolator();
            FixedSpeedScroller scroller = new FixedSpeedScroller(set_viewPager.getContext(), sInterpolator);
            scroller.setmDuration(1000);
            mScroller2.set(set_viewPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createViewPagerForAbTrainingRecovered(View view) {
        set_viewPager = view.findViewById(R.id.enter_ex_set_view_pager);
        ArrayList<String> names = new ArrayList<>();
        for (Exercise e : Savings.workout_sets_to_add) {
            if (!names.contains(e.getName()))
                names.add(e.getName());
        }
        if (names.size() != 2)
            Log.e(LOG_TAG, "Wrong length at recovered exerciser names!");
        SetSlideAdapterRecoveredData slideAdapter = new SetSlideAdapterRecoveredData(getChildFragmentManager(), names.get(0), names.get(1));
        set_viewPager.setAdapter(slideAdapter);
        try {
            Field mScroller3;
            mScroller3 = ViewPager.class.getDeclaredField("mScroller");
            mScroller3.setAccessible(true);
            Interpolator sInterpolator = new DecelerateInterpolator();
            FixedSpeedScroller scroller = new FixedSpeedScroller(set_viewPager.getContext(), sInterpolator);
            scroller.setmDuration(1000);
            mScroller3.set(set_viewPager, scroller);
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
    }

    private void reloadExerciseData(View view) {
        //Catch up with existing data
        for (Exercise e : Savings.workout_sets_to_add) {
            LinearLayout linearLayout;

            if (e.getName().equals(label_text)) {
                linearLayout = view.findViewById(R.id.enter_ex_log_ex_layout);
            } else {
                linearLayout = view.findViewById(R.id.new_ex_enter_choose_ab_log_layout);
                Savings.saved_ab_training_data = true;
                Savings.chosen_ab_training_exercise = e.getName();
            }
           // insertNewSet(view, linearLayout, e.getName(), false);
        }
    }


    private void createViewPagerOneFrag(View view) {
        set_viewPager = view.findViewById(R.id.enter_ex_set_view_pager);
        SetSlideAdapter slideAdapter = new SetSlideAdapter(getChildFragmentManager(), label_text);
        set_viewPager.setAdapter(slideAdapter);
        set_viewPager.setCurrentItem(0);
        reloadExerciseData(view);
    }

    private void createExistingEntries(View view) {

        new Thread(() -> {
            //Waiting for surface to be build
            LinearLayout log_layout = view.findViewById(R.id.enter_ex_log_ex_layout);
            while (log_layout == null) {
                try {
                    Thread.sleep(200);
                    log_layout = view.findViewById(R.id.enter_ex_log_ex_layout);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            int i = 0;
            //if (log_layout == null) return;
            ArrayList<Exercise> list = new ArrayList<>(Savings.workout_sets_to_add);
            Collections.reverse(list);
            for (Exercise exercise : list) {
                LinearLayout ll = null;

                try {
                    ll = createExerciseLog(exercise.getRepeats(), exercise.getWeight(), i);
                } catch (IllegalArgumentException e) {
                    Toast.makeText(getContext(), "No view found 282", Toast.LENGTH_SHORT).show();
                }

                if (ll != null)
                    log_layout.addView(ll);
                i++;
            }
        }).start();

    }



    private void handleValueChange() {
        @SuppressLint("HandlerLeak") Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                tv_current_repeats.setText(String.valueOf(current_repeats));
                tv_current_weight.setText(String.valueOf(current_weight));
            }
        };

        t1 = new Thread(() -> {
            while(true) {
                while (!reps_min_pressed&!reps_plus_pressed&!weight_min_pressed&!weight_plus_pressed) {
                    //Warten
                    try {
                        Thread.sleep(BUTTONS_HOLD_TIME);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }



                if (reps_min_pressed) {
                    if (current_repeats > 0) {
                        current_repeats --;
                    } else {
                        current_repeats = MAX_REPEATS;
                    }
                    handler.sendEmptyMessage(0);
                }

                if (reps_plus_pressed) {
                    if (current_repeats <= MAX_REPEATS) {
                        current_repeats ++;
                    } else {
                        current_repeats = 0;
                    }
                    handler.sendEmptyMessage(0);
                }

                if (weight_min_pressed) {
                    double new_weight = -1;

                    Double[] steps = exerciser.getWeightSteps();
                    for (int i = 0; i < steps.length; i++) {
                        if (current_weight == steps[i]) {
                            if (i == 0) {
                                new_weight = steps[steps.length - 1];
                            } else {
                                new_weight = steps[i-1];
                            }
                        }
                    }
                    current_weight = new_weight;
                    handler.sendEmptyMessage(0);
                }

                if (weight_plus_pressed) {
                    double new_weight = -1;

                    Double[] steps = exerciser.getWeightSteps();
                    for (int i = 0; i < steps.length; i++) {
                        if (current_weight == steps[i]) {
                            if ((i + 1) == steps.length) {
                                new_weight = steps[0];
                            } else {
                                new_weight = steps[i+1];
                            }
                        }
                    }
                    current_weight = new_weight;
                    handler.sendEmptyMessage(0);
                }

                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();


    }

    private void createCompleteButton(View view) {
        Button btn_complete_exercise = view.findViewById(R.id.enter_ex_final_save);
        btn_complete_exercise.setOnClickListener(v -> completeExercise());
    }

    private void completeExercise() {
        saveData();
        resetABTrainingVariables();
        finishFragment();
    }

    private void finishFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this);
        fragmentManager.popBackStack();
        fragmentTransaction.add(R.id.new_exercise_activity_layout, New_Exercise_Start.newInstance(id)).commit();
    }

    private void saveData() {
        ArrayList<Exercise> a_training = new ArrayList<>();
        ArrayList<Exercise> b_training = new ArrayList<>();

        for (Exercise e : Savings.workout_sets_to_add) {
            if (e.getName().equals(label_text)) {
                a_training.add(e);
            } else {
                if (Savings.ab_training_is_active)
                    b_training.add(e);
            }
        }

        //A Training
        Training_Set a_sets = new Training_Set(a_training);
        Savings.workout_added_sets.add(a_sets);

        //B Training
        if (!b_training.isEmpty()) {
            Training_Set b_sets = new Training_Set(b_training);
            Savings.workout_added_sets.add(b_sets);
        }
        //Clear sets to add
        Savings.workout_sets_to_add = new ArrayList<>();
    }

    private void resetABTrainingVariables() {
        Savings.ab_training_is_active = false;
        Savings.chosen_ab_training_exercise = null;
        Savings.chosen_ab_training_category = null;
        Savings.ab_training_category_chosen = false;
    }

    private void createInsertButton(final View view) {
        //INSERT BUTTON
        Button btn_log = view.findViewById(R.id.enter_ex_log_save);
        btn_log.setOnClickListener(v -> {
            LinearLayout log_layout;
            String label;
            if (Savings.ab_training_is_active && set_viewPager.getCurrentItem() == 1) {
                log_layout = view.findViewById(R.id.new_ex_enter_choose_ab_log_layout);
                label = Savings.chosen_ab_training_exercise;
            } else {
                log_layout = view.findViewById(R.id.enter_ex_log_ex_layout);
                label = label_text;
            }

            if (set_viewPager.getCurrentItem() == 1 && !Savings.ab_training_is_active) {
                //Don't insert because B Training isn't chosen yet
            } else {
                insertNewSet(view, log_layout, label, true);
            }
        });
    }

    private void insertNewSet(View view, LinearLayout log_layout, String label, boolean save) {
        final ScrollView scrollView = view.findViewById(R.id.enter_ex_sets_scrollview);
        tv_current_repeats = view.findViewById(R.id.set_num_frag_repeats);
        tv_current_weight= view.findViewById(R.id.set_num_frag_weight);

        //CREATE UI
        int mRepeats = Integer.parseInt("" + tv_current_repeats.getText());
        double mWeight = Double.parseDouble("" + tv_current_weight.getText());
        LinearLayout new_exercise_view = null;

        try {
            new_exercise_view = createExerciseLog(mRepeats, mWeight, Savings.workout_sets_to_add.size());
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        if (new_exercise_view == null) {
            Toast.makeText(getContext(), "No view found - 473" + label, Toast.LENGTH_SHORT).show();
            return;
        }

        if (log_layout == null) {
            Toast.makeText(getContext(), "LogLayout ist null - " + label, Toast.LENGTH_SHORT).show();
            return;
        }
        if (log_layout.getChildCount() == 0 && exerciser.getDefaultWeight() != mWeight) {

            for (Exerciser e : Savings.exercisers_global) {
                if (e.getName().equals(exerciser.getName())) {
                    e.setDefaultWeight(mWeight);
                }
            }
            SharedPreferences.Editor editor = getActivity().getSharedPreferences(Constants.SAVE_TAG_EXERCISER_DEFAULT_WEIGHT, 0).edit();
            editor.putFloat(exerciser.getName(), (float) exerciser.getDefaultWeight());
            editor.apply();
        }


        ArrayList<View> childs = new ArrayList<>();
        for (int i = 0; i < log_layout.getChildCount(); i++) {
            childs.add(log_layout.getChildAt(i));
        }
        childs.add(0, new_exercise_view);
        log_layout.removeAllViews();
        for (View child : childs) log_layout.addView(child);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollBy(0, 0);
            }
        });

        //INSERT DATA
        if (save) {
            Exercise exercise = new Exercise(label, mRepeats, mWeight);
            Savings.workout_sets_to_add.add(exercise);
        }





    }

    private void createBackButton(View view) {
        ImageButton btn_back = view.findViewById(R.id.enter_ex_back_btn);
        btn_back.setOnClickListener(v -> {
            getActivity().onBackPressed();
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createRepeatsButtonMinus(View view) {
        Button btn_repeats_minus = view.findViewById(R.id.set_num_frag_minus_repeats);
        tv_current_repeats = view.findViewById(R.id.set_num_frag_repeats);
        btn_repeats_minus.setOnTouchListener((v, event) -> {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    reps_min_pressed = true;
                    break;
                case MotionEvent.ACTION_UP:
                    reps_min_pressed = false;
            }
            return false;
        });


    }

    @SuppressLint("ClickableViewAccessibility")
    private void createRepeatsButtonPlus(View view) {
        Button btn_repeats_plus = view.findViewById(R.id.set_num_frag_plus_repeats);
        tv_current_repeats = view.findViewById(R.id.set_num_frag_repeats);
        current_repeats = Integer.parseInt(String.valueOf(tv_current_repeats.getText()));
        btn_repeats_plus.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    reps_plus_pressed = true;
                    break;
                case MotionEvent.ACTION_UP:
                    reps_plus_pressed = false;
            }
            return false;
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createWeightButtonMinus(View view) {
        Button btn_weight_minus = view.findViewById(R.id.set_num_frag_minus_weight);
        tv_current_weight= view.findViewById(R.id.set_num_frag_weight);
        current_weight = Double.parseDouble(String.valueOf(tv_current_weight.getText()));
        btn_weight_minus.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    weight_min_pressed = true;
                    break;
                case MotionEvent.ACTION_UP:
                    weight_min_pressed = false;
            }
            return false;
        });

    }

    @SuppressLint("ClickableViewAccessibility")
    private void createWeightButtonPlus(View view) {
        Button btn_weight_plus = view.findViewById(R.id.set_num_frag_plus_weight);
        tv_current_weight= view.findViewById(R.id.set_num_frag_weight);

        //Set default weight
        if (exerciser != null)
            tv_current_weight.setText(String.valueOf(exerciser.getDefaultWeight()));
            btn_weight_plus.setOnTouchListener((v, event) -> {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        weight_plus_pressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        weight_plus_pressed = false;
                }
                return false;
            });
    }

    private LinearLayout createExerciseLog(int repeats, double weight, int pos) throws IllegalArgumentException {

        // CREATE LINEAR LAYOUT
        final LinearLayout LL_log_exercise = new LinearLayout(getContext());
        LL_log_exercise.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        LL_log_exercise.setOrientation(LinearLayout.VERTICAL);
        LL_log_exercise.setId(View.generateViewId());

        New_Filled_Exercise_Element new_filled_exercise_element =
                New_Filled_Exercise_Element.newInstance(repeats, weight, pos);

        //ADD Fragment
        if (set_viewPager == null)
            Log.e(LOG_TAG, "View Pager is null!");

        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(LL_log_exercise.getId(), new_filled_exercise_element)
                .commit();
        return LL_log_exercise;
    }

    private Exerciser findExerciserByName(String name) {
        for (Exerciser e : Savings.exercisers_global) {
            if (e.getName().equals(name)) return e;
        }
        return null;
    }

    private boolean findSavedOwnExerciserByName(String name) {
        SharedPreferences sharedPreferences =
                Objects.requireNonNull(getActivity()).getSharedPreferences(SAVE_TAG_SELF_CREATED_EXERCISER, 0);
        String s = sharedPreferences.getString(name, "-");
        assert s != null;
        return !s.equals("-");
    }
}



