package rohde.fitnesslogger.new_exercise;

import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Training_Set;


public class New_Filled_Set_Element extends Fragment {
    //CONSTANTS
    private static final String NUM_SETS = "num_sets";
    private static final String EXERCISER = "exerciser";
    private static final String EXERCISE = "exercise";
    private static final String TRAININGS_SET_ID = "id";


    //VARS
    private int num_sets;
    private String exerciser;
    private ArrayList<Exercise> exercise_list;
    private long trainings_set_id;


    //UI
    TextView txt_num_sets;
    TextView txt_exerciser;
    TextView txt_num_sets_label;
    ImageButton btn_edit;
    ImageButton btn_del;

    //ME
    Fragment fragment = this;

    public New_Filled_Set_Element() {
        // Required empty public constructor
    }


    public static New_Filled_Set_Element newInstance(long trainings_set_id, int num_sets, String exerciser_name, ArrayList<Exercise> exercise_list) {
        New_Filled_Set_Element fragment = new New_Filled_Set_Element();
        Bundle args = new Bundle();
        args.putInt(NUM_SETS, num_sets);
        args.putString(EXERCISER, exerciser_name);
        args.putString(EXERCISE, exercise_list.toString());
        args.putLong(TRAININGS_SET_ID, trainings_set_id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            num_sets = getArguments().getInt(NUM_SETS);
            exerciser = getArguments().getString(EXERCISER);
            trainings_set_id = getArguments().getLong(TRAININGS_SET_ID);
            String raw_exercise_data = getArguments().getString(EXERCISE);
            if (raw_exercise_data != null)
                exercise_list = Training_Set.getReverseFormat(raw_exercise_data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_element_filled_set, container, false);


        //LOAD UI
        txt_exerciser = view.findViewById(R.id.filled_set_exerciser);
        txt_num_sets = view.findViewById(R.id.filled_set_num_sets);
        txt_num_sets_label = view.findViewById(R.id.filled_set_sets_label);

        if (num_sets == 1)
            txt_num_sets_label.setText("Satz");

        txt_num_sets.setText("" + num_sets);
        txt_exerciser.setText(exerciser);


        //EDIT

        //btn_edit = view.findViewById(R.id.filled_set_btn_edit);
        /*btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Savings.edit_exercise = new ArrayList<>(); //Clear
                Savings.edit_exercise = exercise_list;
                Savings.edit_set_id = trainings_set_id;

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.add(R.id.main_frame_layout,
                        New_Enter_Exercise_Fragment.newInstance(exerciser, new Training_Set(exercise_list)));
                fragmentTransaction.commit();

            }
        });*/

        btn_del = view.findViewById(R.id.filled_set_btn_del);
        btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Training_Set temp_set = null;
                ArrayList<Training_Set> temp = new ArrayList<>(Savings.workout_added_sets);
                for (Training_Set set : temp) {
                    if (set.getId() == trainings_set_id) {
                        temp_set = set;
                        Savings.workout_added_sets.remove(set);
                    }
                }
                final Training_Set deleted_set = temp_set;

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentTransaction.detach(fragment).commit();

                Snackbar.make(Objects.requireNonNull(getView()), R.string.exercise_del, Snackbar.LENGTH_LONG)
                        .setAction(R.string.recover_exercise, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Savings.workout_added_sets.add(deleted_set);
                                FragmentManager fragmentManager = getFragmentManager();
                                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                                fragmentTransaction.attach(fragment).commit();
                            }
                        })
                        .show();

            }
        });
        return view;
    }


}
