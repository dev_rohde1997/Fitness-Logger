package rohde.fitnesslogger.new_exercise;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Set;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Category;
import rohde.fitnesslogger.saving_classes.Exerciser;


public class New_Exercise_Enter_Choose_AB extends Fragment {

    Fragment fragment = this;

    Thread t;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_exercise_enter_choose_ab, container, false);

        LinearLayout add_layout_left = view.findViewById(R.id.new_ex_enter_choose_ab_main_add_right);
        LinearLayout add_layout_right = view.findViewById(R.id.new_ex_enter_choose_ab_main_add_left);

        addCategoryElements(add_layout_left, add_layout_right);

        @SuppressLint("HandlerLeak") Handler category_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                add_layout_left.removeAllViews();
                add_layout_right.removeAllViews();

                Set<String> exerciser_list = getExerciserNameList();
                addExerciserElements(exerciser_list, add_layout_left, add_layout_right);
            }
        };

        @SuppressLint("HandlerLeak") Handler exercise_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                //Layouts
                LinearLayout selection_layout = view.findViewById(R.id.new_ex_enter_choose_ab_selection_layout);
                LinearLayout sets_layout = view.findViewById(R.id.new_ex_enter_choose_ab_enter_sets_layout);

                selection_layout.setVisibility(View.GONE);
                sets_layout.setVisibility(View.VISIBLE);

                //Headline
                TextView headline = view.findViewById(R.id.new_ex_enter_choose_ab_main_headline);
                headline.setText(Savings.chosen_ab_training_exercise);



            }
        };

        t = new Thread(() -> {

            while (true) {
                //Skip category selection if b training data from recovery exists
                if (Savings.saved_ab_training_data) {
                    Savings.ab_training_category_chosen = true;
                    Savings.chosen_ab_training_category = "";
                }

                if (Savings.chosen_ab_training_category != null && !Savings.ab_training_category_chosen) {
                    //Change to exercise selection
                    category_handler.sendEmptyMessage(0);
                    Savings.ab_training_category_chosen = true;
                } else if (Savings.chosen_ab_training_exercise != null && !Savings.ab_training_is_active) {
                    exercise_handler.sendEmptyMessage(0);
                    Savings.ab_training_is_active = true;
                }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();


        return view;
    }

    private void addCategoryElements(LinearLayout add_layout_left, LinearLayout add_layout_right) {
        int pos = 0;
        for (Category category : Savings.exercise_muscles_categories) {
            if (pos == 0) {
                add_layout_left.addView(createCategoryElement(category.getName(), category.getResourceImageID()));
                pos++;
            } else {
                add_layout_right.addView(createCategoryElement(category.getName(), category.getResourceImageID()));
                pos--;
            }
        }
    }

    @Nullable
    private Set<String> getExerciserNameList() {
        Set<String> exerciser_list = null;

        //Abs
        if (Savings.chosen_ab_training_category.equals(getString(R.string.abs) )) {
            exerciser_list = Savings.ex_abs.keySet();
        }
        //Chest
        if (Savings.chosen_ab_training_category.equals(getString(R.string.chest) )) {
            exerciser_list = Savings.ex_chest.keySet();
        }
        //Arms
        if (Savings.chosen_ab_training_category.equals(getString(R.string.arms) )) {
            exerciser_list = Savings.ex_arms.keySet();
        }
        //Shoulder
        if (Savings.chosen_ab_training_category.equals(getString(R.string.shoulders) )) {
            exerciser_list = Savings.ex_shoulders.keySet();
        }
        //Glutes
        if (Savings.chosen_ab_training_category.equals(getString(R.string.glutes) )) {
            exerciser_list = Savings.ex_glutes.keySet();
        }
        //Back
        if (Savings.chosen_ab_training_category.equals(getString(R.string.back) )) {
            exerciser_list = Savings.ex_back.keySet();
        }
        //Quads
        if (Savings.chosen_ab_training_category.equals(getString(R.string.quads) )) {
            exerciser_list = Savings.ex_quads.keySet();
        }
        //Hamstrings
        if (Savings.chosen_ab_training_category.equals(getString(R.string.hamstrings) )) {
            exerciser_list = Savings.ex_hamstrings.keySet();
        }
        //Calves
        if (Savings.chosen_ab_training_category.equals(getString(R.string.calves) )) {
            exerciser_list = Savings.ex_calves.keySet();
        }

        return exerciser_list;
    }

    private void addExerciserElements(Set<String> exerciser_list, LinearLayout add_layout_left, LinearLayout add_layout_right) {
        int pos = 0;
        for (Exerciser e : Savings.exercisers_global) {
            for (String s : exerciser_list) {
                if (e.getName().equals(Savings.current_exerciser))
                    continue;

                if (e.getName().equals(s)) {
                    if (pos == 0) {
                        add_layout_left.addView(createExerciserElement(e.getName(), e.getImage()));
                        pos++;
                    } else {
                        add_layout_right.addView(createExerciserElement(e.getName(), e.getImage()));
                        pos--;
                    }
                }
            }
        }
    }

    private LinearLayout createCategoryElement(String name, int img) {

        // CREATE LINEAR LAYOUT
        final LinearLayout LL_log_exercise = new LinearLayout(getContext());
        LL_log_exercise.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        LL_log_exercise.setOrientation(LinearLayout.VERTICAL);
        LL_log_exercise.setId(View.generateViewId());

        New_Exercise_Enter_Choose_AB_Categories new_exercise_enter_choose_ab_categories =
                New_Exercise_Enter_Choose_AB_Categories.newInstance(name, img);

        //ADD Fragment
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(LL_log_exercise.getId(), new_exercise_enter_choose_ab_categories)
                .commit();
        return LL_log_exercise;
    }

    private LinearLayout createExerciserElement(String name, int img) {

        // CREATE LINEAR LAYOUT
        final LinearLayout LL_log_exercise = new LinearLayout(getContext());
        LL_log_exercise.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        LL_log_exercise.setOrientation(LinearLayout.VERTICAL);
        LL_log_exercise.setId(View.generateViewId());

        New_Exercise_Enter_Choose_AB_Element new_exercise_enter_choose_ab_element =
                New_Exercise_Enter_Choose_AB_Element.newInstance(name, img);

        //ADD Fragment
        FragmentManager fragmentManager = getChildFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(LL_log_exercise.getId(), new_exercise_enter_choose_ab_element)
                .commitAllowingStateLoss();
        return LL_log_exercise;
    }

}