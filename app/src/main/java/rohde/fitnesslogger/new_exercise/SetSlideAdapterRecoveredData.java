package rohde.fitnesslogger.new_exercise;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;


public class SetSlideAdapterRecoveredData extends FragmentStatePagerAdapter {

    String text = "";
    String text2 = "";

    public SetSlideAdapterRecoveredData(FragmentManager fm, String text, String text2) {
        super(fm);
        this.text = text;
        this.text2 = text2;
    }



    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: return New_Exercise_Enter_Sets.newInstance(text);
            case 1: return New_Exercise_Enter_Sets.newInstance(text2);
            default: return null;
        }
    }
}