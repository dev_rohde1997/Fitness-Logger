package rohde.fitnesslogger.new_exercise;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Training_Set;
import rohde.fitnesslogger.saving_classes.Workout;
import rohde.fitnesslogger.saving_classes.Workout_plan;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;


public class New_Exercise_Start extends Fragment {

    public static final String WORKOUT = "workout";
    public static final String EXERCISE_NAME = "name";

    //UI
    ImageButton btn_back;
    Button btn_new_exercise;
    Button btn_save;
    LinearLayout linearLayoutExerciseList;
    Fragment fragment = this;
    TextView tv_headline;
    boolean skip_once = true;

    //Vars
    long workout_plan_id;
    String exercise_name = null;

    public static New_Exercise_Start newInstance(long workout_plan_id) {
        New_Exercise_Start fragment = new New_Exercise_Start();
        Bundle args = new Bundle();
        args.putLong(WORKOUT, workout_plan_id);
        fragment.setArguments(args);
        return fragment;
    }

    public static New_Exercise_Start newInstance(long workout_plan_id, String exercise_name) {
        New_Exercise_Start fragment = new New_Exercise_Start();
        Bundle args = new Bundle();
        args.putLong(WORKOUT, workout_plan_id);
        args.putString(EXERCISE_NAME, exercise_name);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            workout_plan_id = getArguments().getLong(WORKOUT);
            exercise_name = getArguments().getString(EXERCISE_NAME);
            Savings.current_workout_plan_id = workout_plan_id;
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_exercise_start, container, false);


        //Building UI
        tv_headline = view.findViewById(R.id.new_ex_start_headline_text);
        tv_headline.setText(findNameById(workout_plan_id));

        //Back
        btn_back = view.findViewById(R.id.new_ex_start_back_btn);
        btn_back.setOnClickListener(v -> {
            Objects.requireNonNull(getActivity()).onBackPressed();
        });


        //NEW EXERCISE BUTTON
        btn_new_exercise = view.findViewById(R.id.new_ex_start_btn_new_exercise);
        btn_new_exercise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction
                        .replace(R.id.new_exercise_activity_layout, New_Exercise_By_Plan.newInstance(workout_plan_id))
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack(null)
                        .commit();
            }
        });

        //SAVE BUTTON
        btn_save = view.findViewById(R.id.new_ex_start_btn_save);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    if (Savings.workout_added_sets.size() != 0) {

                        Date currentTime = Calendar.getInstance().getTime();
                        Workout w = new Workout(currentTime.toString(), new ArrayList<>(Savings.workout_added_sets));
                        Savings.workouts.add(w);

                        //LOG
                        //Save workout constantly
                        SharedPreferences.Editor editor = Objects.requireNonNull(getActivity()).getSharedPreferences("Training", 0).edit();
                        editor.putString(currentTime.toString(), w.toString());
                        editor.apply();
                        Savings.workout_added_sets.clear();
                        Savings.current_workout_plan_id = -1;


                        getActivity().finish();
                    }
                } catch (Exception e) {
                    Toast.makeText(getContext(), R.string.error_saving_data, Toast.LENGTH_LONG).show();
                }
            }
        });

        //No Exercises
        TextView tv = view.findViewById(R.id.new_ex_start_no_ex_tv);
        if (Savings.workout_added_sets.size() == 0) {
            tv.setVisibility(View.VISIBLE);
            if (skip_once) {
                skip_once = false;
                btn_new_exercise.callOnClick();
            }
        } else {
            tv.setVisibility(View.GONE);
        }

        //LADE EINGETRAGENE ÜBUNGEN
        linearLayoutExerciseList = view.findViewById(R.id.new_training_frag_list_layout);
        createTrainingElements();

        //Open Enter View if this view is recovered
        if (exercise_name != null) {
            Log.i(LOG_TAG, "Continue");
            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.new_exercise_activity_layout, New_Exercise_Enter.newInstance(exercise_name, workout_plan_id));
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }

        //SKIP LAYOUT IF 0 EXERCISES


        return view;
    }

    private void createTrainingElements() {

        linearLayoutExerciseList.removeAllViews();

        for (Training_Set t : Savings.workout_added_sets) {
            if (t.getExercises().size() == 0) {
                continue;
            }
            linearLayoutExerciseList.addView(createTrainingsSetElement(t));
        }
    }

    private LinearLayout createTrainingsSetElement(Training_Set t) {
        //CREATE LINEAR LAYOUT
        final LinearLayout LL_log_exercise = new LinearLayout(getContext());
        LL_log_exercise.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        LL_log_exercise.setOrientation(LinearLayout.VERTICAL);
        LL_log_exercise.setId(View.generateViewId());

        New_Filled_Set_Element new_filled_set_element =
                New_Filled_Set_Element.newInstance(t.getId(), t.getExercises().size(),
                        t.getExercises().get(0).getName(), t.getExercises());

        //ADD Fragment
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(LL_log_exercise.getId(), new_filled_set_element)
                .commit();
        return LL_log_exercise;
    }

    private String findNameById(long id) {
        for (Workout_plan wp : Savings.workout_plan) {
            if (wp.getId() == id) return wp.getName();
        }
        return null;
    }


}
