package rohde.fitnesslogger.new_exercise;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link New_Exercise_Enter_Choose_AB_Element#newInstance} factory method to
 * create an instance of this fragment.
 */
public class New_Exercise_Enter_Choose_AB_Element extends Fragment {

    private static final String LABEL_TXT = "text";
    private static final String IMAGE_SRC = "src";
    private static final String ID = "id";

    private String label_text;
    private int image_src;

    public static New_Exercise_Enter_Choose_AB_Element newInstance(String text, int image) {
        New_Exercise_Enter_Choose_AB_Element fragment = new New_Exercise_Enter_Choose_AB_Element();
        Bundle args = new Bundle();
        args.putString(LABEL_TXT, text );
        args.putInt(IMAGE_SRC, image);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            label_text = getArguments().getString(LABEL_TXT);
            image_src = getArguments().getInt(IMAGE_SRC);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_exercise_enter_choose_ab_element, container, false);

        final ImageView image = view.findViewById(R.id.new_ex_enter_choose_ab_element_image);
        final TextView headline = view.findViewById(R.id.new_ex_enter_choose_ab_element_text);

        final RelativeLayout layout = view.findViewById(R.id.new_ex_enter_choose_ab_element_layout);
        layout.setOnClickListener(v -> {
            Savings.chosen_ab_training_exercise = label_text;
        });

        headline.setText(label_text);
        image.setImageResource(image_src);

        return view;
    }
}