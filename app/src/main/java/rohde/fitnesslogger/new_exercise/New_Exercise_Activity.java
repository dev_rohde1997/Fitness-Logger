package rohde.fitnesslogger.new_exercise;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import java.util.ArrayList;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Training_Set;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;
import static rohde.fitnesslogger.saving_classes.Constants.SAVE_TAG_TEMP_WORKOUT_DATA;
import static rohde.fitnesslogger.saving_classes.Constants.TAG_EXERCISE_NOT_SAVED;
import static rohde.fitnesslogger.saving_classes.Constants.TAG_WORKOUT_NOT_SAVED;
import static rohde.fitnesslogger.saving_classes.Constants.TAG_WORKOUT_NOT_SAVED_ID;

public class New_Exercise_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_exercise);

        try {
            FrameLayout frameLayout = findViewById(R.id.new_exercise_activity_layout);
            FragmentManager fragmentManager = getSupportFragmentManager();
            @SuppressLint("CommitTransaction") FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (Savings.recoverSavedData) {
                SharedPreferences preferences_exercises = getSharedPreferences(SAVE_TAG_TEMP_WORKOUT_DATA, 0);
                String unsaved_workout_data = preferences_exercises.getString(TAG_WORKOUT_NOT_SAVED, null);
                String unsaved_training_set_data = preferences_exercises.getString(TAG_EXERCISE_NOT_SAVED, null);
                long unsaved_workout_plan_id = preferences_exercises.getLong(TAG_WORKOUT_NOT_SAVED_ID, -1);

                /*Log.i(LOG_TAG, "Unsaved: " + unsaved_workout_data);
                Log.i(LOG_TAG, "Unsaved: " + unsaved_training_set_data);
                Log.i(LOG_TAG, "Unsaved: " + unsaved_workout_plan_id);*/



                ArrayList<Training_Set> temp_training_set = new ArrayList<>();

                //Rebuild data
                assert unsaved_workout_data != null;
                if (!unsaved_workout_data.equals("[]")) {
                    String[] unsaved_training_sets = unsaved_workout_data.split("], ");


                    for (String training_set : unsaved_training_sets) {
                        ArrayList<Exercise> temp_exercise_set = new ArrayList<>();
                        training_set = training_set.replace("[", "");
                        training_set = training_set.replace("]", "");
                        String[] training_exercises = training_set.split(", ");
                        for (String parts : training_exercises) {
                            temp_exercise_set.add(new Exercise(parts.split("\\|")));
                        }
                        temp_training_set.add(new Training_Set(temp_exercise_set));
                    }
                }

                String name = null;
                if (unsaved_training_set_data != null && !unsaved_training_set_data.equals("[]")) {
                    ArrayList<Exercise> temp_set = new ArrayList<>();
                    unsaved_training_set_data = unsaved_training_set_data.replace("[", "");
                    unsaved_training_set_data = unsaved_training_set_data.replace("]", "");
                    String[] exerciser = unsaved_training_set_data.split(", ");
                    for (String part : exerciser) {
                        temp_set.add(new Exercise(part.split("\\|")));
                        name = part.split("\\|")[0];
                    }
                    Savings.workout_sets_to_add = temp_set;
                }

                Savings.workout_added_sets = temp_training_set;

    //            Log.i(LOG_TAG, "Recovered: " + Savings.workout_added_sets.toString());

                if (name != null) {
                    fragmentTransaction.add(frameLayout.getId(), New_Exercise_Start.newInstance(unsaved_workout_plan_id, name));
                } else {
                    fragmentTransaction.add(frameLayout.getId(), New_Exercise_Start.newInstance(unsaved_workout_plan_id));
                }
                preferences_exercises.edit().clear().apply();
                Savings.recoverSavedData = false;
            } else {
                fragmentTransaction
                        .add(frameLayout.getId(), new New_Exercise_Menu());
            }







            fragmentTransaction.commit();
        } catch (ClassCastException e) {
            // Creating the Channel
            NotificationManager notificationManager =
                    (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationChannel channel = new NotificationChannel("default",
                    "Channel name",
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Channel description");
            notificationManager.createNotificationChannel(channel);

            Notification n= new Notification.Builder(this,"default")
                    .setContentTitle(getPackageName())
                    .setContentText("Class Cast Exception!")
                    .setBadgeIconType(R.mipmap.ic_launcher)
                    .setNumber(5)
                    .setSmallIcon(R.mipmap.ic_launcher_round)
                    .setAutoCancel(true)
                    .build();

            notificationManager.notify(1, n);
        }

    }

    @Override
    protected void onPause() {
        SharedPreferences.Editor editor = getSharedPreferences(SAVE_TAG_TEMP_WORKOUT_DATA, 0).edit();
        editor.putLong(TAG_WORKOUT_NOT_SAVED_ID, Savings.current_workout_plan_id);

        if (Savings.workout_added_sets != null)
            editor.putString(TAG_WORKOUT_NOT_SAVED, Savings.workout_added_sets.toString());

        if (Savings.workout_sets_to_add != null)
            editor.putString(TAG_EXERCISE_NOT_SAVED, Savings.workout_sets_to_add.toString());

        editor.apply();

        super.onPause();
    }

}
