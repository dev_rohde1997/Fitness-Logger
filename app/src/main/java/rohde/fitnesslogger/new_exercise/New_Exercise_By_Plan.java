package rohde.fitnesslogger.new_exercise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Workout_plan;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;


public class New_Exercise_By_Plan extends Fragment {

    private static final String ID = "ID";

    long id;
    ImageButton btn_back;

    public static New_Exercise_By_Plan newInstance(long id) {
        New_Exercise_By_Plan fragment = new New_Exercise_By_Plan();
        Bundle args = new Bundle();
        args.putLong(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getLong(ID);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_new_exercise_by_plan, container, false);

        //Set headline text
        setHeadlineText(view);

        //New Exercise
        createBackButton(view);
        createSurface(view);

        return view;
    }

    private void setHeadlineText(View view) {
        final TextView headline = view.findViewById(R.id.new_ex_by_plan_headline);
        if (id == -1) {
            headline.setText(findNameById(R.string.new_workout));
        } else {
            headline.setText(findNameById(id));
        }
    }

    private void createSurface(View view) {
        LinearLayout left_layout = view.findViewById(R.id.new_ex_by_plan_add_layout_left);
        LinearLayout right_layout = view.findViewById(R.id.new_ex_by_plan_add_layout_right);

        ArrayList<Exerciser> temp_exercise_list = choseExerciseListById();
        if (temp_exercise_list == null) {
            Log.e(LOG_TAG, "New_Exercise_By_Plan - Fehler beim Suchen der Exercise by ID");
            return;
        }

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //Add views
        boolean left = true;
        LinearLayout add_layout;
        Collections.sort(temp_exercise_list);
        for (Exerciser e : temp_exercise_list) {
            if (left) {
                add_layout = left_layout;
                left = false;
            } else {
                add_layout = right_layout;
                left = true;
            }
            New_Exerciser_Element new_exerciser_element = createExerciseElement(e.getName(), e.getImage());
            fragmentTransaction
                    .add(add_layout.getId(), new_exerciser_element);
        }


        fragmentTransaction.commit();
    }

    private ArrayList<Exerciser> choseExerciseListById() {
        ArrayList<Exerciser> temp_exercise_list;

        if (id == -1) {
            temp_exercise_list = new ArrayList<>(Savings.exercisers_global);
        } else {
            Workout_plan workout_plan = findWorkoutPlan(id);
            if (workout_plan == null) {
                return null;
            }
            temp_exercise_list = workout_plan.getExerciser();
        }
        return temp_exercise_list;
    }

    private void createBackButton(View view) {
        btn_back = view.findViewById(R.id.new_ex_by_plan_back_btn);
        btn_back.setOnClickListener(v -> getActivity().onBackPressed());
    }

    private String findNameById(long id) {
        for (Workout_plan wp : Savings.workout_plan) {
            if (wp.getId() == id) return wp.getName();
        }
        return null;
    }

    private New_Exerciser_Element createExerciseElement(String name, int image) {
        return New_Exerciser_Element.newInstance(name, image, id);
    }


    private Workout_plan findWorkoutPlan(long id) {
        for (Workout_plan wp : Savings.workout_plan) {
            if (wp.getId() == id) return wp;
        }
        return null;
    }



}
