package rohde.fitnesslogger.new_exercise;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exercise;


public class New_Filled_Exercise_Element extends Fragment {
    //CONST
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    //VARS
    private int repeats;
    private double weight;
    private int pos;

    //UI
    ImageButton btn_delete;
    TextView txt_Reps;
    TextView txt_Weight;
    LinearLayout layout;

    //ME
    Fragment fragment = this;

    public New_Filled_Exercise_Element() {
        // Required empty public constructor
    }


    public static New_Filled_Exercise_Element newInstance(int repeats, double weight, int pos) {
        New_Filled_Exercise_Element fragment = new New_Filled_Exercise_Element();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, repeats);
        args.putDouble(ARG_PARAM2, weight);
        args.putInt(ARG_PARAM3, pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            repeats = getArguments().getInt(ARG_PARAM1);
            weight  = getArguments().getDouble(ARG_PARAM2);
            pos  = getArguments().getInt(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_element_filled_exercise, container, false);

        btn_delete = view.findViewById(R.id.filled_ex_edit);
        txt_Weight = view.findViewById(R.id.filled_ex_weight);
        txt_Reps = view.findViewById(R.id.filled_ex_repeats);

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (Exercise e : Savings.workout_sets_to_add) {
                    if (e.getRepeats() == repeats && e.getWeight() == weight) {
                        Savings.workout_sets_to_add.remove(e);
                        break;
                    }
                }

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.remove(fragment).commit();

            }
        });

        txt_Reps.setText(String.valueOf(repeats));
        txt_Weight.setText(String.valueOf(weight));

        return view;
    }


}
