package rohde.fitnesslogger.new_workout_plan;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;


public class New_Workout_Plan_Exerciser_Element extends Fragment {

   RelativeLayout master_layout;

    private static final String NAME = "param1";
    private static final String IMG = "param2";

    private String name;
    private int image;
    private Exerciser mExerciser;
    private boolean selected = false;

    public New_Workout_Plan_Exerciser_Element() {
        // Required empty public constructor
    }

    public static New_Workout_Plan_Exerciser_Element newInstance(String name, int image) {
        New_Workout_Plan_Exerciser_Element fragment = new New_Workout_Plan_Exerciser_Element();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putInt(IMG, image);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            image = getArguments().getInt(IMG);
        }

        for (Exerciser e : Savings.exercisers_global) {
            if (e.getName().equals(name)) {
                mExerciser = e;
                break;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_new_workout_plan_exerciser_element, container, false);

        TextView textView = view.findViewById(R.id.new_workout_plan_element_name);
        ImageView imageView = view.findViewById(R.id.new_workout_plan_element_image);
        ImageView check_image = view.findViewById(R.id.new_workout_plan_element_image_check);
        master_layout = view.findViewById(R.id.new_workout_plan_exerciser_element_main_layout);

        //Set text and image
        textView.setText(name);
        imageView.setImageResource(image);


        master_layout.setOnClickListener(v -> {
            if (selected) {
                Savings.workout_plan_exercisers_in_plan.remove(mExerciser);
                Savings.workout_plan_new_exerciser_to_add.remove(mExerciser);
                check_image.setVisibility(View.GONE);
            } else {
                Savings.workout_plan_new_exerciser_to_add.add(0, mExerciser);
                Savings.workout_plan_exercisers_in_plan.add(0, mExerciser);
                check_image.setVisibility(View.VISIBLE);
            }
            selected = !selected;
            Savings.changeAddExList = true;

        });




        return view;
    }
}
