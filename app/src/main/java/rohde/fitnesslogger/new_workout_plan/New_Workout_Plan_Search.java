package rohde.fitnesslogger.new_workout_plan;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;

import static rohde.fitnesslogger.main.Savings.exercisers_global;
import static rohde.fitnesslogger.main.Savings.workout_plan_new_exerciser_to_add;

/**
 * A simple {@link Fragment} subclass.
 */
public class New_Workout_Plan_Search extends Fragment {

    public New_Workout_Plan_Search() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_workout_plan_search, container, false);

        super.onCreate(savedInstanceState);

        EditText editText = view.findViewById(R.id.new_workout_plan_search_edit_text);
        LinearLayout add_layout = view.findViewById(R.id.new_workout_plan_search_add_layout);


        editText.requestFocus();
        editText.setInputType(InputType.TYPE_CLASS_TEXT);
        editText.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            editText.setCursorVisible(false);
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                //sendMessage();

                handled = true;
            }
            return handled;
        });

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                add_layout.removeAllViews();
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                a: for (Exerciser e : exercisers_global) {
                    for (Exerciser e1 : Savings.workout_plan_exercisers_in_plan)
                        if (e1.getName().equals(e.getName()))
                            continue a;
                    if (e.getName().contains(s)) {
                        New_Workout_Plan_Exerciser_Element fragment = New_Workout_Plan_Exerciser_Element.newInstance(e.getName(), e.getImage());
                        fragmentTransaction.add(add_layout.getId(), fragment);
                    }
                }
                fragmentTransaction.commit();
            }
        });

        Button add_button = view.findViewById(R.id.new_workout_plan_search_add_btn);
        add_button.setOnClickListener(v -> {

            workout_plan_new_exerciser_to_add.clear();

            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.new_workout_plan_activity_layout, New_Workout_Plan_Overview.newInstance());
            fragmentTransaction.commit();

        });

        ImageButton back_btn = view.findViewById(R.id.new_workout_plan_search_headline_back_btn);
        back_btn.setOnClickListener(v -> getActivity().onBackPressed());

        return view;
    }
}
