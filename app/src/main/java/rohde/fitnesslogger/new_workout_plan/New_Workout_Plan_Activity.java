package rohde.fitnesslogger.new_workout_plan;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.FrameLayout;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.new_exercise.IOnBackPressed;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

public class New_Workout_Plan_Activity extends AppCompatActivity {

    String workout_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_workout_plan_activity);

        workout_name = getIntent().getStringExtra("NAME");

        FrameLayout frameLayout = findViewById(R.id.new_workout_plan_activity_layout);
        FragmentManager fragmentManager = getSupportFragmentManager();
        @SuppressLint("CommitTransaction") FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(frameLayout.getId(), New_Workout_Plan_Overview.newInstance(workout_name)).commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.new_workout_plan_activity_layout);
        if (!(fragment instanceof IOnBackPressed) || !((IOnBackPressed) fragment).onBackPressed()) {
            super.onBackPressed();
        }
    }
}
