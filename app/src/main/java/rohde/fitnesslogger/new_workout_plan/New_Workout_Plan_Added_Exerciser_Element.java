package rohde.fitnesslogger.new_workout_plan;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;

public class New_Workout_Plan_Added_Exerciser_Element extends Fragment {

    private static final String NAME = "name";
    private static final String SIZE = "size";
    private static final int SIZE_THRESHOLD = 18;

    private String name;
    private int size;

    public New_Workout_Plan_Added_Exerciser_Element() {
        // Required empty public constructor
    }

    public static New_Workout_Plan_Added_Exerciser_Element newInstance(String name, int text_size) {
        New_Workout_Plan_Added_Exerciser_Element fragment = new New_Workout_Plan_Added_Exerciser_Element();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putInt(SIZE, text_size);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            size = getArguments().getInt(SIZE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_workout_plan_added_exerciser_element, container, false);

        //TextView
        TextView tv_text = view.findViewById(R.id.new_workout_plan_added_ex_element_text);
        tv_text.setText(name);

        //Text size
        if (size >= SIZE_THRESHOLD) size = SIZE_THRESHOLD;
        tv_text.setTextSize(size);

        //Del Btn
        ImageView del_btn = view.findViewById(R.id.new_workout_plan_added_ex_element_del);
        del_btn.setOnClickListener(v -> {

            //Remove element in list
            ArrayList<Exerciser> list = new ArrayList<>(Savings.workout_plan_exercisers_in_plan);
            for (Exerciser e : list) {
                if (e.getName().equals(name)) {
                    Savings.workout_plan_exercisers_in_plan.remove(e);
                    break;
                }
            }

            //Remove view
            ((ViewGroup)view.getParent().getParent()).removeView((ViewGroup)view.getParent());
        });

        return view;
    }
}
