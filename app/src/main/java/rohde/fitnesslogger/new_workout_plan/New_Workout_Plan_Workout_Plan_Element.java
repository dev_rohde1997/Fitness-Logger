package rohde.fitnesslogger.new_workout_plan;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Constants;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Muscles;
import rohde.fitnesslogger.saving_classes.Workout;
import rohde.fitnesslogger.saving_classes.Workout_plan;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;


public class New_Workout_Plan_Workout_Plan_Element extends Fragment {
    private static final String ID = "id";
    private static final String NAME = "name";

    private long id;
    private String name;
    boolean hold = false;
    int progressStatus = 0;
    Handler handler = new Handler();

    //UI
    ProgressBar progressBar;
    TextView tv_name;
    ImageView front_image;
    ImageView back_image;

    public New_Workout_Plan_Workout_Plan_Element() {
        // Required empty public constructor
    }


    public static New_Workout_Plan_Workout_Plan_Element newInstance(long id, String name) {
        New_Workout_Plan_Workout_Plan_Element fragment = new New_Workout_Plan_Workout_Plan_Element();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putLong(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            id = getArguments().getLong(ID);
            name = getArguments().getString(NAME);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_workout_plan_workout_plan_element, container, false);
        progressBar = view.findViewById(R.id.new_workout_plan_element_progress_bar);
        tv_name = view.findViewById(R.id.new_workout_plan_element_headline_text);
        front_image = view.findViewById(R.id.new_workout_plan_front_image);
        back_image = view.findViewById(R.id.new_workout_plan_back_image);
        tv_name.setText(name);
        LinearLayout linearLayout = view.findViewById(R.id.new_workout_plan_element_list);
        RelativeLayout main_layout = view.findViewById(R.id.new_workout_plan_overview_bottom_layout);

        progress();

        createDeleteButton(view);

        createAndAssignOnClickListener(linearLayout, main_layout);

        createExerciserUIList(linearLayout);

        createMuscleModel();

        return view;
    }

    private void createExerciserUIList(LinearLayout linearLayout) {
        Workout_plan myWorkoutPlan = null;
        for (Workout_plan wp : Savings.workout_plan) {
            if (wp.getName().equals(name)) {
                myWorkoutPlan = wp;
            }
        }

        if (myWorkoutPlan != null) {
            ArrayList<Exerciser> temp_ex = new ArrayList<>();
            temp_ex.addAll(myWorkoutPlan.getExerciser());
            Collections.sort(temp_ex);
            for (Exerciser e : temp_ex) {
                TextView textView = new TextView(getContext());
                textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                textView.setText(e.getName());
                textView.setPadding(25,5,5,5);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(15);
                linearLayout.addView(textView);
            }
        }
    }

    private void createAndAssignOnClickListener(LinearLayout linearLayout, RelativeLayout main_layout) {
        View.OnClickListener onClickListener = v -> {
            Savings.workout_plan_exercisers_in_plan.clear();
            Intent intent = new Intent(getContext(), New_Workout_Plan_Activity.class);
            intent.putExtra("NAME", name);
            startActivity(intent);
        };


        main_layout.setOnClickListener(onClickListener);
        tv_name.setOnClickListener(onClickListener);
        linearLayout.setOnClickListener(onClickListener);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void createDeleteButton(View view) {
        ImageButton del_btn = view.findViewById(R.id.new_workout_plan_del_btn);

        del_btn.setOnTouchListener((v, event) -> {
            int action = event.getAction();
            switch (action) {
                case MotionEvent.ACTION_DOWN:
                    startProgress();
                    break;
                case MotionEvent.ACTION_UP:
                    stopProgress();

                    break;
            }


            return false;
        });
    }

    private void progress() {
        new Thread(() -> {
            while (true) {
                if (progressStatus < 100 & hold) {
                    // Update the progress bar and display the
                        progressStatus += 1;
                    //current value in the text view
                    handler.post(() -> progressBar.setProgress(progressStatus));
                    try {
                        // Sleep for 200 milliseconds.
                        Thread.sleep(7);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (progressStatus == 100) removeElement();
                }
            }
        }).start();
    }

    private void startProgress() {
        hold = true;
    }

    private void stopProgress() {
        progressStatus = 0;
        hold = false;
        handler.post(() -> progressBar.setProgress(progressStatus));
    }

    private void removeElement() {
        //Local
        ArrayList<Workout_plan> list = new ArrayList<>(Savings.workout_plan);
        for (Workout_plan w : list) {
            if (w.getId() == id) {
                Savings.workout_plan.remove(w);
                break;
            }
        }


        //Global
        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor =
                Objects.requireNonNull(getActivity()).
                        getSharedPreferences(Constants.SAVE_TAG_WORKOUT_PLAN, 0).edit();

        editor.remove(""+id).apply();

        //Remove Surface Object
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.remove(this).commit();
    }

    private void createMuscleModel() {

        //Get Muscle List
        Workout_plan mPlan = null;
        for (Workout_plan wp : Savings.workout_plan) {
            if (wp.getId() == id) {
                mPlan = wp;
                break;
            }
        }

        if (mPlan == null) return;



        // Create Muscle List
        Set<String> muscles = new HashSet<>();
        for (Exerciser e : mPlan.getExerciser()) {
            muscles.addAll(Arrays.asList(e.getMuscleGroups()));
        }
        String[] muscle_array = new String[muscles.size()];
        muscles.toArray(muscle_array);

        //Front Muscle
        ArrayList<Drawable> front_muscle_image_list = new ArrayList<>();
        Drawable front_image_drawable = getResources().getDrawable(R.drawable.body_model_front, Objects.requireNonNull(getActivity()).getTheme());
        front_muscle_image_list.add(front_image_drawable);
        front_muscle_image_list.addAll(getDrawables(muscle_array, Muscles.front_muscles_with_resId));
        //Create Drawable Layers
        Drawable[] front_layers = new Drawable[front_muscle_image_list.size()];
        LayerDrawable front_layers_drawable = new LayerDrawable(front_muscle_image_list.toArray(front_layers));
        front_image.setImageDrawable(front_layers_drawable);

        //Back Muscle
        ArrayList<Drawable> back_muscle_image_list = new ArrayList<>();
        Drawable back_image_drawable = getResources().getDrawable(R.drawable.body_model_back, Objects.requireNonNull(getActivity()).getTheme());
        back_muscle_image_list.add(back_image_drawable);
        back_muscle_image_list.addAll(getDrawables(muscle_array, Muscles.back_muscles_with_resId));
        Drawable[] back_layers = new Drawable[back_muscle_image_list.size()];
        LayerDrawable back_layers_drawable = new LayerDrawable(back_muscle_image_list.toArray(back_layers));
        back_image.setImageDrawable(back_layers_drawable);
    }

    @NonNull
    private ArrayList<Drawable> getDrawables(String[] muscle_array, HashMap<String, Integer> map) {
        ArrayList<Drawable> muscle_image_list = new ArrayList<>();


        for (String s : muscle_array) {
            Integer resID = map.get(s);

            //No muscle image found in front muscles
            if (resID == null) {
                resID = map.get(s);
            }

            if (resID != null) {
                Drawable layer = getResources().getDrawable(resID, Objects.requireNonNull(getActivity()).getTheme());
                muscle_image_list.add(layer);
            }
        }
        return muscle_image_list;
    }

}
