package rohde.fitnesslogger.new_workout_plan;

import android.annotation.SuppressLint;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;


public class New_Workout_Plan_Add_Exercise extends Fragment {

    private static final String NAME = "name";

    private String name;

    public New_Workout_Plan_Add_Exercise() {
        // Required empty public constructor
    }


    public static New_Workout_Plan_Add_Exercise newInstance(String name) {
        New_Workout_Plan_Add_Exercise fragment = new New_Workout_Plan_Add_Exercise();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_workout_plan_add_exercise, container, false);

        LinearLayout add_layout = view.findViewById(R.id.new_workout_plan_add_ex);
        //Find all exerciser

        ImageButton back_btn = view.findViewById(R.id.new_workout_plan_add_ex_back_btn);
        back_btn.setOnClickListener(v -> Objects.requireNonNull(getActivity()).onBackPressed());

        addExerciserMuscleElements(add_layout);

        TextView tv_no_ex_selected = view.findViewById(R.id.new_workout_plan_add_ex_no_ex_selected_text);
        Button add_exerciser_btn = view.findViewById(R.id.new_workout_plan_add_ex_add_btn);
        AtomicBoolean wait_for_sleep = new AtomicBoolean(false);
        add_exerciser_btn.setOnClickListener( v -> {


            if (Savings.workout_plan_new_exerciser_to_add.size() == 0) {
                if (!wait_for_sleep.get()) {
                    tv_no_ex_selected.setVisibility(View.VISIBLE);
                    wait_for_sleep.set(true);
                    @SuppressLint("HandlerLeak") Handler handler = new Handler() {
                        @Override
                        public void handleMessage(Message msg) {
                            //GONE
                            tv_no_ex_selected.setVisibility(View.GONE);
                        }
                    };
                    new Thread(() -> {
                        try {
                            if (wait_for_sleep.get())
                                Thread.sleep(3500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        wait_for_sleep.set(false);
                        handler.sendEmptyMessage(0);
                    }).start();
                }
            } else {
                Savings.workout_plan_new_exerciser_to_add.clear();
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.new_workout_plan_activity_layout, New_Workout_Plan_Overview.newInstance());
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    private void addExerciserMuscleElements(LinearLayout add_layout) {
        ArrayList<Exerciser> exercisers = new ArrayList<>();
        Collection<Exerciser> mList = getCategoryMuscleListByName(name);

        if (mList == null) return;

        a: for (Exerciser e : mList) {
            //Check is exercise is already in list
            for (Exerciser e1 : Savings.workout_plan_exercisers_in_plan) {
                if (e.equals(e1))
                    continue a;
            }
            exercisers.add(e);
        }
        createElements(add_layout, exercisers);
    }


    private void createElements(LinearLayout add_layout, ArrayList<Exerciser> exercisers) {
        Set<Exerciser> set = new HashSet<>(exercisers);
        exercisers.clear();
        exercisers.addAll(set);

        Collections.sort(exercisers);
        //Add Exerciser
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        for (Exerciser e : exercisers) {
            fragmentTransaction.add(add_layout.getId(), New_Workout_Plan_Exerciser_Element.newInstance(e.getName(), e.getImage()));
        }
        fragmentTransaction.commit();
    }

    @Nullable
    private Collection<Exerciser> getCategoryMuscleListByName(String name) {
        Collection<Exerciser> exerciser_list = null;

        //Abs
        if (name.equals(getString(R.string.abs) )) {
            exerciser_list = Savings.ex_abs.values();
        }
        //Chest
        if (name.equals(getString(R.string.chest) )) {
            exerciser_list = Savings.ex_chest.values();
        }
        //Arms
        if (name.equals(getString(R.string.arms) )) {
            exerciser_list = Savings.ex_arms.values();
        }
        //Shoulder
        if (name.equals(getString(R.string.shoulders) )) {
            exerciser_list = Savings.ex_shoulders.values();
        }
        //Glutes
        if (name.equals(getString(R.string.glutes) )) {
            exerciser_list = Savings.ex_glutes.values();
        }
        //Back
        if (name.equals(getString(R.string.back) )) {
            exerciser_list = Savings.ex_back.values();
        }
        //Quads
        if (name.equals(getString(R.string.quads) )) {
            exerciser_list = Savings.ex_quads.values();
        }
        //Hamstrings
        if (name.equals(getString(R.string.hamstrings) )) {
            exerciser_list = Savings.ex_hamstrings.values();
        }
        //Calves
        if (name.equals(getString(R.string.calves) )) {
            exerciser_list = Savings.ex_calves.values();
        }

        return exerciser_list;
    }



}
