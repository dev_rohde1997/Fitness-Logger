package rohde.fitnesslogger.new_workout_plan;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collections;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Workout_plan;

import static android.view.View.GONE;
import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class New_Workout_Plan_Menu extends Fragment {

    LinearLayout enter_name_layout;
    LinearLayout default_button_layout;
    EditText editText;

    RelativeLayout workout_plan_layout;
    LinearLayout workout_plan_layout_add;

    TextView tv_name_exists;

    AlphaAnimation alphaAnimation;

    public New_Workout_Plan_Menu() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_workout_plan_menu, container, false);


        //own_ex_layout = view.findViewById(R.id.new_workout_plan_menu_manage_own_exercises_layout);
        workout_plan_layout = view.findViewById(R.id.new_workout_plan_menu_manage_workout_plan_layout);

        workout_plan_layout_add = view.findViewById(R.id.new_workout_plan_menu_workout_plan_layout);
        //own_ex_layout_add = view.findViewById(R.id.new_workout_plan_menu_own_exercise_layout);

        tv_name_exists = view.findViewById(R.id.new_workout_plan_menu_no_ex_selected_text);

        enter_name_layout = view.findViewById(R.id.new_workout_plan_menu_enter_name_layout);
        default_button_layout = view.findViewById(R.id.new_workout_plan_menu_create_plan_layout);
        editText = createEditText(view);

        createNewWorkoutButton(view);
        createCancelButton(view);
        createContinueButton(view);


        //loadOwnExerciser();
        //loadWorkoutPlanElements();

        return view;
    }

   /* private void loadOwnExerciser() {
        own_ex_layout_add.removeAllViews();

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Collections.sort(Savings.self_created_exerciser);

        for (Exerciser e: Savings.self_created_exerciser) {
            fragmentTransaction.add(own_ex_layout_add.getId(), New_Workout_Plan_Own_Exerciser_Element.newInstance(e.getName()));

        }
        fragmentTransaction.commit();
    }*/


    private void createNewWorkoutButton(View view) {
        Button new_workout_plan_btn = view.findViewById(R.id.new_workout_plan_menu_create_plan);
        new_workout_plan_btn.setOnClickListener(v -> {
            enter_name_layout.setVisibility(View.VISIBLE);
            default_button_layout.setVisibility(GONE);
            editText.requestFocus();

        });
    }

    @NonNull
    private EditText createEditText(View view) {
        EditText editText = view.findViewById(R.id.new_workout_plan_menu_edit_text);
        editText.setInputType(InputType.TYPE_CLASS_TEXT);

        editText.setOnEditorActionListener((v, actionId, event) -> {
            boolean handled = false;
            editText.setCursorVisible(false);
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                //sendMessage();

                handled = true;
            }
            return handled;
        });
        editText.setOnClickListener(v -> {
            editText.setCursorVisible(true);

        });
        return editText;
    }

    private void createContinueButton(View view) {
        Button continue_btn = view.findViewById(R.id.new_workout_plan_menu_enter_name_continue);
        AtomicBoolean wait_for_sleep = new AtomicBoolean(false);

        continue_btn.setOnClickListener(v -> {
            for (Workout_plan w : Savings.workout_plan) {
                if (w.getName().equals(""+editText.getText())) {
                    if (!wait_for_sleep.get()) {
                        tv_name_exists.setVisibility(View.VISIBLE);
                        wait_for_sleep.set(true);
                        @SuppressLint("HandlerLeak") Handler handler = new Handler() {
                            @Override
                            public void handleMessage(Message msg) {
                                tv_name_exists.setVisibility(View.GONE);
                            }
                        };
                        new Thread(() -> {
                            try {
                                if (wait_for_sleep.get())
                                    Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            wait_for_sleep.set(false);
                            handler.sendEmptyMessage(0);
                        }).start();
                    }
                    return;
                }
            }
            Intent intent = new Intent(getContext(), New_Workout_Plan_Activity.class);
            intent.putExtra("NAME", "" + editText.getText());
            Savings.workout_plan_exercisers_in_plan.clear();
            Savings.workout_plan_new_exerciser_to_add.clear();
            editText.setText("");
            enter_name_layout.setVisibility(GONE);
            default_button_layout.setVisibility(View.VISIBLE);
            startActivity(intent);
        });
    }

    private void createCancelButton(View view) {
        Button cancel_btn = view.findViewById(R.id.new_workout_plan_menu_enter_name_cancel);
        cancel_btn.setOnClickListener(v -> {
            enter_name_layout.setVisibility(GONE);
            default_button_layout.setVisibility(View.VISIBLE);
            editText.setText("");
            // Check if no view has focus:
            InputMethodManager imm = (InputMethodManager) Objects.requireNonNull(getActivity())
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            //own_ex_layout.setVisibility(View.VISIBLE);
            workout_plan_layout.setVisibility(View.VISIBLE);
        });
    }

    private void loadWorkoutPlanElements() {

        workout_plan_layout_add.removeAllViews();

        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Collections.sort(Savings.workout_plan);
        for (Workout_plan w : Savings.workout_plan) {
            fragmentTransaction.add(workout_plan_layout_add.getId(), New_Workout_Plan_Workout_Plan_Element.newInstance(w.getId(), w.getName()));
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onResume() {
        //loadOwnExerciser();
        loadWorkoutPlanElements();
        super.onResume();
    }
}
