package rohde.fitnesslogger.new_workout_plan;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Constants;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Muscles;
import rohde.fitnesslogger.saving_classes.Workout_plan;
import rohde.fitnesslogger.util.ViewWeightAnimationWrapper;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;


public class New_Workout_Plan_Overview extends Fragment {

    TextView tv_no_elements;
    ImageView front_image;
    ImageView back_image;
    ImageView background;
    RelativeLayout button_layout;
    Fragment fragment = this;

    //ADD Layout
    GridLayout add_layout;
    LinearLayout add_layout_wrap;
    RelativeLayout human_layout;
    LinearLayout human_layout_wrap;
    View splitter;
    List<LinearLayout> layoutList = new ArrayList<>();
    List<View> horizontal_splitter = new ArrayList<>();

    boolean waitForResizing = false;
    volatile boolean thread_alive = true;
    Thread t;

    String name;
    public static final String NAME = "NAME";

    public New_Workout_Plan_Overview() {
        // Required empty public constructor
    }

    public static New_Workout_Plan_Overview newInstance(String name) {
        New_Workout_Plan_Overview fragment = new New_Workout_Plan_Overview();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        fragment.setArguments(args);
        return fragment;
    }

    public static New_Workout_Plan_Overview newInstance() {
        return new New_Workout_Plan_Overview();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            Savings.current_workout_plan_name = name;
        } else {
            name = Savings.current_workout_plan_name;
        }

        //Load data
        if (Savings.workout_plan_exercisers_in_plan.size() == 0) {
            for (Workout_plan w : Savings.workout_plan) {
                if (w.getName().equals(name) & w.getExerciser() != null) {
                    Savings.workout_plan_exercisers_in_plan = new ArrayList<>(w.getExerciser());
                    break;
                }
            }
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_workout_plan_overview, container, false);

        //Init
        add_layout = view.findViewById(R.id.new_workout_plan_overview_added_ex_layout);
        add_layout_wrap = view.findViewById(R.id.new_workout_plan_overview_added_ex_layout_wrap);
        front_image = view.findViewById(R.id.new_workout_plan_front_image);
        back_image = view.findViewById(R.id.new_workout_plan_back_image);
        tv_no_elements = view.findViewById(R.id.new_workout_plan_overview_add_ex_placeholder_text);
        background = view.findViewById(R.id.new_workout_plan_overview_background);
        human_layout = view.findViewById(R.id.new_workout_plan_overview_humans_layout);
        human_layout_wrap = view.findViewById(R.id.new_workout_plan_overview_humans_layout_wrap);

        //IMAGES
        //front_image.setImageResource(R.drawable.body_front);
        //back_image.setImageResource(R.drawable.body_back);

        createAddExerciserButton(view);
        createCancelButton(view);
        createSaveButton(view);
        createMuscleModel();
        createUIUpdateThread();

        return view;
    }

    private void createUIUpdateThread() {
        //Resize Thread for added exerciser elements
        @SuppressLint("HandlerLeak") Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                checkGridSize();
                createMuscleModel();
            }
        };

        t = new Thread(() -> {
            while(thread_alive) {
                if (!waitForResizing)
                    handler.sendEmptyMessage(0);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t.start();
    }

    private void createSaveButton(View view) {
        Button save_btn = view.findViewById(R.id.new_workout_plan_overview_save_btn);
        save_btn.setOnClickListener(v -> {
            saveWorkoutPlan();
        });
    }

    private void createCancelButton(View view) {
        Button cancel_btn = view.findViewById(R.id.new_workout_plan_overview_add_ex_abort);
        cancel_btn.setOnClickListener(v -> {
            Savings.workout_plan_exercisers_in_plan.clear();
            Objects.requireNonNull(getActivity()).finish();
        });
    }

    //TODO: PERFORMANCE ISSUE!!!
    private void createMuscleModel() {
        // Create Muscle List
        Set<String> muscles = new HashSet<>();
        for (Exerciser e : Savings.workout_plan_exercisers_in_plan) {
            muscles.addAll(Arrays.asList(e.getMuscleGroups()));
        }
        String[] muscle_array = new String[muscles.size()];
        muscles.toArray(muscle_array);

        //Front Muscle
        ArrayList<Drawable> front_muscle_image_list = new ArrayList<>();
        Drawable front_image_drawable = getResources().getDrawable(R.drawable.body_model_front, Objects.requireNonNull(getActivity()).getTheme());
        front_muscle_image_list.add(front_image_drawable);
        front_muscle_image_list.addAll(getDrawables(muscle_array, Muscles.front_muscles_with_resId));
        //Create Drawable Layers
        Drawable[] front_layers = new Drawable[front_muscle_image_list.size()];
        LayerDrawable front_layers_drawable = new LayerDrawable(front_muscle_image_list.toArray(front_layers));
        front_image.setImageDrawable(front_layers_drawable);

        //Back Muscle
        ArrayList<Drawable> back_muscle_image_list = new ArrayList<>();
        Drawable back_image_drawable = getResources().getDrawable(R.drawable.body_model_back, Objects.requireNonNull(getActivity()).getTheme());
        back_muscle_image_list.add(back_image_drawable);
        back_muscle_image_list.addAll(getDrawables(muscle_array, Muscles.back_muscles_with_resId));
        Drawable[] back_layers = new Drawable[back_muscle_image_list.size()];
        LayerDrawable back_layers_drawable = new LayerDrawable(back_muscle_image_list.toArray(back_layers));
        back_image.setImageDrawable(back_layers_drawable);
    }

    private void saveWorkoutPlan() {
        if (Savings.workout_plan_exercisers_in_plan.size() == 0) return;

        ArrayList<Exerciser> exercisers = new ArrayList<>(Savings.workout_plan_exercisers_in_plan);
        Savings.workout_plan_exercisers_in_plan.clear();
        Workout_plan wp = null;
        for (Workout_plan w : Savings.workout_plan) {
            if (w.getName().equals(name)) {
                wp = w;
                w.setExerciser(exercisers);
                break;
            }
        }
        if (wp == null) {
            wp = new Workout_plan(Calendar.getInstance().getTimeInMillis(), name, exercisers);
            Savings.workout_plan.add(wp);
        }


        @SuppressLint("CommitPrefEdits") SharedPreferences.Editor editor =
                Objects.requireNonNull(getActivity()).getSharedPreferences(
                        Constants.SAVE_TAG_WORKOUT_PLAN, 0).edit();
        editor.putString("" + wp.getId(), wp.toString());
        editor.apply();

        getActivity().finish();
    }

    @NonNull
    private ArrayList<Drawable> getDrawables(String[] muscle_array, HashMap<String, Integer> map) {
        ArrayList<Drawable> muscle_image_list = new ArrayList<>();


        for (String s : muscle_array) {
            Integer resID = map.get(s);

            //No muscle image found in front muscles
            if (resID == null) {
                resID = map.get(s);
            }

            if (resID != null) {
                Drawable layer = getResources().getDrawable(resID, Objects.requireNonNull(getActivity()).getTheme());
                muscle_image_list.add(layer);
            }
        }
        return muscle_image_list;
    }

    @Override
    public void onResume() {
        loadTempTrainingPlan();
        super.onResume();
    }

    private void createAddExerciserButton(View view) {
        Button add_ex_btn = view.findViewById(R.id.new_workout_plan_overview_add_ex_btn);
        add_ex_btn.setOnClickListener(v -> {
            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            @SuppressLint("CommitTransaction") FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.new_workout_plan_activity_layout, new New_Workout_Plan_Filter_Exercise())
            .addToBackStack(null).commit();
        });
    }

    private void loadTempTrainingPlan() {

        if (Savings.workout_plan_exercisers_in_plan.size() > 0) {
            tv_no_elements.setVisibility(View.GONE);
        } else {
            tv_no_elements.setVisibility(View.VISIBLE);
            return;
        }

        add_layout.removeAllViews();

        for (Exerciser e : Savings.workout_plan_exercisers_in_plan) {
            add_layout.addView(createFragmentInLayout(e.getName(), Savings.workout_plan_exercisers_in_plan.size()));
        }

        checkGridSizeAfterStartOnce();


    }

    private void checkGridSize() {
        float add_size_goal = 0;
        float add_size = ((LinearLayout.LayoutParams) add_layout_wrap.getLayoutParams()).weight ;
        float human_size_goal = 0;
        float human_size = ((LinearLayout.LayoutParams) human_layout_wrap.getLayoutParams()).weight ;

        //if (add_size == add_size_goal)

        if (Savings.workout_plan_exercisers_in_plan.size() <= 2) {
            add_size_goal = 8.5f;
            human_size_goal = 1.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 4) {
            add_size_goal = 7.5f;
            human_size_goal = 2.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 6) {
            add_size_goal = 6.5f;
            human_size_goal = 3.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 8) {
            add_size_goal = 5.5f;
            human_size_goal = 4.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 10) {
            add_size_goal = 4.5f;
            human_size_goal = 5.5f;
        } else {
            add_size_goal = 3f;
            human_size_goal = 7f;
        }



        waitForResizing = true;

        ViewWeightAnimationWrapper animationWrapper = new ViewWeightAnimationWrapper(add_layout_wrap);
        ObjectAnimator anim = ObjectAnimator.ofFloat(animationWrapper,
                "weight",
                animationWrapper.getWeight(),
                add_size_goal);
        anim.setDuration(500);
        anim.start();

        ViewWeightAnimationWrapper animationWrapper2 = new ViewWeightAnimationWrapper(human_layout_wrap);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(animationWrapper2,
                "weight",
                animationWrapper2.getWeight(),
                human_size_goal);
        anim2.setDuration(500);
        anim2.start();

        new Thread(() -> {
            while (anim2.isRunning() || anim.isRunning()) {
                //Wait
            }
            waitForResizing = false;
        }).start();

        //add_layout_wrap.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 3.0f ));
        //human_layout_wrap.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, 7.0f ));
    }

    private void checkGridSizeAfterStartOnce() {
        float add_size_goal = 0;
        float add_size = ((LinearLayout.LayoutParams) add_layout_wrap.getLayoutParams()).weight ;
        float human_size_goal = 0;
        float human_size = ((LinearLayout.LayoutParams) human_layout_wrap.getLayoutParams()).weight ;

        if (Savings.workout_plan_exercisers_in_plan.size() <= 2) {
            add_size_goal = 8.5f;
            human_size_goal = 1.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 4) {
            add_size_goal = 7.5f;
            human_size_goal = 2.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 6) {
            add_size_goal = 6.5f;
            human_size_goal = 3.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 8) {
            add_size_goal = 5.5f;
            human_size_goal = 4.5f;
        } else if (Savings.workout_plan_exercisers_in_plan.size() <= 10) {
            add_size_goal = 4.5f;
            human_size_goal = 5.5f;
        } else {
            add_size_goal = 3f;
            human_size_goal = 7f;
        }

        add_layout_wrap.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, add_size_goal ));
        human_layout_wrap.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, human_size_goal ));
    }

    private LinearLayout createFragmentInLayout(String name, int scale) {
        // CREATE LINEAR LAYOUT
        final LinearLayout linearLayout = new LinearLayout(getContext());

        GridLayout.LayoutParams params= new GridLayout.LayoutParams(GridLayout.spec(
                GridLayout.UNDEFINED,GridLayout.FILL,1f),
                GridLayout.spec(GridLayout.UNDEFINED,GridLayout.FILL,1f));
        params.height = 0;
        params.width = 0;
        linearLayout.setLayoutParams(params);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setId(View.generateViewId());

        int size_default = 180;
        New_Workout_Plan_Added_Exerciser_Element f =
                New_Workout_Plan_Added_Exerciser_Element.newInstance(name, size_default / scale);

        //ADD Fragment
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(linearLayout.getId(), f)
                .commit();
        return linearLayout;
    }

    private int getImageResourceByName(String aString) {
        String packageName = getActivity().getPackageName();
        return getResources().getIdentifier(aString, "drawable", packageName);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        thread_alive = false;
    }





}
