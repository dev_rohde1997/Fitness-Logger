package rohde.fitnesslogger.new_workout_plan.filter_muscle_view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SlideAdapterFilterMuscle extends FragmentStatePagerAdapter {

//    String text = "";



    public SlideAdapterFilterMuscle(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Fragment getItem(int i) {
        return new Fragment_Muscle();
    }
}