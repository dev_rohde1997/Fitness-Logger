package rohde.fitnesslogger.new_workout_plan.filter_muscle_view;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SlideAdapterFilterExercise extends FragmentStatePagerAdapter {

//    String text = "";



    public SlideAdapterFilterExercise(FragmentManager fm) {
        super(fm);

    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: return new Fragment_Muscle();
            case 1: return new Fragment_Exerciser();
            default: return null;
        }
    }
}