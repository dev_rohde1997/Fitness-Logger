package rohde.fitnesslogger.new_workout_plan.filter_muscle_view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Exerciser}.
 */
public class RecyclerViewAdapter_Exerciser_Filter extends RecyclerView.Adapter<RecyclerViewAdapter_Exerciser_Filter.ViewHolder> {

    private final List<Exerciser> exercisers;

    public RecyclerViewAdapter_Exerciser_Filter(Collection<Exerciser> items) {
        exercisers = new ArrayList<>(items);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_exerciser, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = exercisers.get(position);
        holder.mImageView.setImageResource(exercisers.get(position).getImage());
        holder.mTextview.setText(exercisers.get(position).getName());
    }

    public void updateData(ArrayList<Exerciser> viewModels) {
        exercisers.clear();
        exercisers.addAll(viewModels);
        notifyDataSetChanged();
    }

    public void addItem(int position, Exerciser exerciser) {
        exercisers.add(position, exerciser);
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        exercisers.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public int getItemCount() {
        return exercisers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTextview;
        public Exerciser mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.exerciser_fragment_item_image);
            mTextview = (TextView) view.findViewById(R.id.exerciser_fragment_item_name);

            mView.setOnClickListener(v -> {

                //Set global vars for inter adapter communication
                Savings.workout_plan_new_exerciser_to_add.add(0, mItem);
                Savings.workout_plan_content_to_add_changed = true;

                //Remove item from adapter
                removeItem(getAdapterPosition());
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextview.getText() + "'";
        }
    }
}