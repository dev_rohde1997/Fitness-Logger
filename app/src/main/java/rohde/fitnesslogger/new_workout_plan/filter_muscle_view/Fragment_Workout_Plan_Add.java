package rohde.fitnesslogger.new_workout_plan.filter_muscle_view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

/**
 * A fragment representing a list of Items.
 */
public class Fragment_Workout_Plan_Add extends Fragment {

    private int mColumnCount = 1;
    private RecyclerViewAdapter_Workout_Plan_Add adapter;
    private boolean running = true;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Fragment_Workout_Plan_Add() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_muscle_list, container, false);



        // Set the adapter

        RecyclerView recyclerView = (RecyclerView) view;
        LinearLayoutManager linearLayout = new LinearLayoutManager(getContext());
        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayout);

        adapter = new RecyclerViewAdapter_Workout_Plan_Add(Savings.workout_plan_new_exerciser_to_add);
        recyclerView.setAdapter(adapter);


        @SuppressLint("HandlerLeak")
        Handler slide_adapter_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                adapter.notifyDataSetChanged();
            }
        };

        Thread thread = new Thread(() -> {
            while (running) {
                if (Savings.workout_plan_content_to_add_changed) {
                    slide_adapter_handler.sendEmptyMessage(0);
                    Savings.workout_plan_content_to_add_changed = false;
                }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();


        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        running = false;
    }
}