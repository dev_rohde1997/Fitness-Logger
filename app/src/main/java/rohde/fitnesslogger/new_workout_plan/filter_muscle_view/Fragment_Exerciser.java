package rohde.fitnesslogger.new_workout_plan.filter_muscle_view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.service.autofill.SaveInfo;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Exerciser;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

/**
 * A fragment representing a list of Items.
 */
public class Fragment_Exerciser extends Fragment {

    private int mColumnCount = 2;
    private boolean running = true;
    private RecyclerViewAdapter_Exerciser_Filter adapter = null;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public Fragment_Exerciser() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_muscle_list, container, false);

        RecyclerView recyclerView = (RecyclerView) view;

        recyclerView.setLayoutManager(new GridLayoutManager(view.getContext(), mColumnCount));


        ArrayList<Exerciser> available_exercisers_in_category = new ArrayList<>();
        if (Savings.chosen_muscle_category != null) {

            //Search for available exercisers in this category to display
            for (Exerciser e : Savings.chosen_muscle_category.values()) {
                if (!Savings.workout_plan_new_exerciser_to_add.contains(e) && !Savings.workout_plan_exercisers_in_plan.contains(e))
                    available_exercisers_in_category.add(e);
            }

            adapter = new RecyclerViewAdapter_Exerciser_Filter(available_exercisers_in_category);
            recyclerView.setAdapter(adapter);
        }

        @SuppressLint("HandlerLeak")
        Handler slide_adapter_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                //Search for available exercisers in this category to display
                available_exercisers_in_category.clear();
                for (Exerciser e : Savings.chosen_muscle_category.values()) {
                    if (!Savings.workout_plan_new_exerciser_to_add.contains(e) && !Savings.workout_plan_exercisers_in_plan.contains(e))
                        available_exercisers_in_category.add(e);
                }
                adapter.updateData(available_exercisers_in_category);
            }
        };

        Thread thread = new Thread(() -> {
            while (running) {
                if (Savings.workout_plan_exerciser_to_display_changed) {
                    slide_adapter_handler.sendEmptyMessage(0);
                    Savings.workout_plan_exerciser_to_display_changed = false;
                }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();





        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        running = false;
    }
}