package rohde.fitnesslogger.new_workout_plan.filter_muscle_view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Category;
import rohde.fitnesslogger.saving_classes.Exerciser;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Exerciser}.
 */
public class RecyclerViewAdapter_Muscle_Filter extends RecyclerView.Adapter<RecyclerViewAdapter_Muscle_Filter.ViewHolder> {

    private final List<Category> mValues;

    public RecyclerViewAdapter_Muscle_Filter(List<Category> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_muscle, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mImageView.setImageResource(mValues.get(position).getResourceImageID());
        holder.mTextview.setText(mValues.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mImageView;
        public final TextView mTextview;
        public Category mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mImageView = (ImageView) view.findViewById(R.id.muscle_fragment_item_image);
            mTextview = (TextView) view.findViewById(R.id.muscle_fragment_item_name);

            view.setOnClickListener(v -> {
                Savings.chosen_muscle_category = mItem.getExercisers();
                Savings.chosen_muscle_category_changed = true;
            });
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTextview.getText() + "'";
        }
    }
}