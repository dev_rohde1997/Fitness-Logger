package rohde.fitnesslogger.new_workout_plan;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exerciser;


public class New_Workout_Plan_Selected_Element extends Fragment {

    private static final String NAME = "param1";
    private static final String IMG = "param2";


    private String name;
    private int image;

    TextView tv_name;
    ImageView imageView;

    public static New_Workout_Plan_Selected_Element newInstance(String name, int image) {
        New_Workout_Plan_Selected_Element fragment = new New_Workout_Plan_Selected_Element();
        Bundle args = new Bundle();
        args.putString(NAME, name);
        args.putInt(IMG, image);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            name = getArguments().getString(NAME);
            image = getArguments().getInt(IMG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_new_workout_plan_selected_element, container, false);

        //Init
        tv_name = view.findViewById(R.id.new_workout_plan_selected_name);
        imageView = view.findViewById(R.id.new_workout_plan_selected_image);



        tv_name.setText(name);
        imageView.setImageResource(image);

        imageView.setOnClickListener(v -> {
            ArrayList<Exerciser> temp = new ArrayList<>(Savings.workout_plan_exercisers_in_plan);
            for (Exerciser e : temp) {
                if (e.getName().equals(name)) {
                    Savings.workout_plan_exercisers_in_plan.remove(e);
                }
            }
            Savings.refresh_selected_list = true;
        });


        return view;
    }
}
