package rohde.fitnesslogger.new_workout_plan;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Objects;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.new_exercise.IOnBackPressed;
import rohde.fitnesslogger.new_workout_plan.filter_muscle_view.SlideAdapterFilterExercise;
import rohde.fitnesslogger.new_workout_plan.filter_muscle_view.SlideAdapterFilterMuscle;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.util.FixedSpeedScroller;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;


public class New_Workout_Plan_Filter_Exercise extends Fragment implements IOnBackPressed{


    //GridLayout mGridLayout;
    ViewPager pager;
    boolean running = true;
    boolean isExerciserViewActice = false;
    Button add_button;

    public New_Workout_Plan_Filter_Exercise() {
        // Required empty public constructor
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_new_workout_plan_filter_exercise, container, false);
        pager = view.findViewById(R.id.new_workout_plan_filter_view_pager);
        add_button = view.findViewById(R.id.new_workout_plan_filter_add_button);
        add_button.setOnClickListener(v -> {

            //Add chosen and finish surface
            ArrayList<Exerciser> temp = new ArrayList<>(Savings.workout_plan_new_exerciser_to_add);
            Savings.workout_plan_exercisers_in_plan.addAll(temp);
            Savings.workout_plan_new_exerciser_to_add.clear();
            FragmentManager fragmentManager = getFragmentManager();
            if (fragmentManager != null)
                fragmentManager.popBackStackImmediate();
            else
                Objects.requireNonNull(getActivity()).finish();
        });



        //Define pager

        //createSearchView(view);

        createDefaultScroller();
        createSmoothScrollerHandler();

        return view;
    }

    private void createDefaultScroller() {
        SlideAdapterFilterMuscle adapter = new SlideAdapterFilterMuscle(getFragmentManager());
        pager.setAdapter(adapter);
        buildScroller();
    }

    /** Create a dynamic slide adapter
     * Handler and Thread are defined for communication between thread and ui
     */
    private void createSmoothScrollerHandler() {
        @SuppressLint("HandlerLeak") Handler update_chosen_item_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                pager.setCurrentItem(pager.getChildCount()-1);
            }
        };


        @SuppressLint("HandlerLeak")
        Handler slide_adapter_handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (Savings.chosen_muscle_category == null)
                    pager.setAdapter(new SlideAdapterFilterMuscle(getFragmentManager()));
                else {
                    pager.setAdapter(new SlideAdapterFilterExercise(getFragmentManager()));
                }
                buildScroller();
            }
        };

        Thread thread = new Thread(() -> {
            boolean activateSlide = false;
            while (running) {
                if (Savings.chosen_muscle_category_changed) {
                    slide_adapter_handler.sendEmptyMessage(0);
                    Savings.chosen_muscle_category_changed = false;
                    activateSlide = true;
                }

                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (activateSlide) {
                    update_chosen_item_handler.sendEmptyMessage(0);
                    activateSlide = false;
                }
            }
        });
        thread.start();
    }

    /** Build the a Scroll Element to control the scroll with a smooth movement
     *
     */
    private void buildScroller() {
        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            Interpolator sInterpolator = new DecelerateInterpolator();
            FixedSpeedScroller scroller = new FixedSpeedScroller(pager.getContext(), sInterpolator);
            scroller.setmDuration(1000);
            mScroller.set(pager, scroller);
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException ignored) {
        }
    }



    /*private void createSearchView(View view) {
        RelativeLayout search_layout = view.findViewById(R.id.new_workout_plan_filter_search_layout);
        search_layout.setOnClickListener(v -> {
            FragmentManager fragmentManager = getFragmentManager();
            assert fragmentManager != null;
            @SuppressLint("CommitTransaction") FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.new_workout_plan_activity_layout, new New_Workout_Plan_Search())
                    .addToBackStack(null).commit();

        });
    }*/



    @Override
    public void onDestroy() {
        super.onDestroy();
        running = false;
    }

    @Override
    public boolean onBackPressed() {
        if (pager.getCurrentItem() == 1) {
            pager.setCurrentItem(0);
            Savings.chosen_muscle_category = null;
            return true;
        }
        return false;
    }
}


