package rohde.fitnesslogger.results;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.HomeScreen_Fragment;


public class Result_Menu extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_menu, container, false);
        ImageButton back_btn = view.findViewById(R.id.results_menu_back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(new HomeScreen_Fragment());
            }
        });

        Button btn_graph = view.findViewById(R.id.results_menu_btn_graph);
        btn_graph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(new Result_Exercise_List());
            }
        });

        Button btn_last = view.findViewById(R.id.results_menu_btn_last);
        btn_last.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), R.string.error_not_impl, Toast.LENGTH_SHORT).show();
            }
        });

        Button btn_raw = view.findViewById(R.id.results_menu_btn_raw);
        btn_raw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFragment(new Results_Raw());
            }
        });


        return view;
    }

    private void openFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .replace(R.id.result_activity_layout, fragment)
                .addToBackStack(null)
                .commit();
    }
}
