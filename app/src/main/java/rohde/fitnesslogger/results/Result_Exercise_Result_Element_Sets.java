package rohde.fitnesslogger.results;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import rohde.fitnesslogger.R;


public class Result_Exercise_Result_Element_Sets extends Fragment {
    private static final String WEIGHT = "weight";
    private static final String REPS = "reps";
    private static final String TIME = "time";

    private double weight;
    private int reps;
    private long time;

    public Result_Exercise_Result_Element_Sets() {
        // Required empty public constructor
    }


    public static Result_Exercise_Result_Element_Sets newInstance(double weight, int reps, long time) {
        Result_Exercise_Result_Element_Sets fragment = new Result_Exercise_Result_Element_Sets();
        Bundle args = new Bundle();
        args.putDouble(WEIGHT, weight);
        args.putInt(REPS, reps);
        args.putLong(TIME, time);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            weight = getArguments().getDouble(WEIGHT);
            reps = getArguments().getInt(REPS);
            time = getArguments().getLong(TIME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_result_exercise_result_element_sets, container, false);

        //Reps
        setValue(view, R.id.result_ex_result_element_repeats, String.valueOf(reps));

        //Weight
        setValue(view, R.id.result_ex_result_element_weight, String.valueOf(weight));


        return view;
    }

    private void setValue(View view, int id, String text) {
        TextView tv_time = view.findViewById(id);
        tv_time.setText(text);
    }



    private String getTime(long timestamp) {
        Date date = new Date(timestamp);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("hh:mm");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    private String getDay(long timestamp) {
        Date date = new Date(timestamp);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("E");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }
}
