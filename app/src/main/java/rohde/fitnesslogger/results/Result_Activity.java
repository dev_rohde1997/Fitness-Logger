package rohde.fitnesslogger.results;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import java.util.Objects;

import rohde.fitnesslogger.R;

public class Result_Activity extends AppCompatActivity {

    private static final String LABEL_TXT = "text";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        String name = Objects.requireNonNull(getIntent().getExtras()).getString(LABEL_TXT);

        FrameLayout frameLayout = findViewById(R.id.result_activity_layout);
        FragmentManager fragmentManager = getSupportFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(frameLayout.getId(), Result_Graph.newInstance(name)).commit();
    }
}
