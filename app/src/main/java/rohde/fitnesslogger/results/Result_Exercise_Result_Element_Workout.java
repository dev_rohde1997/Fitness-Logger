package rohde.fitnesslogger.results;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Training_Set;
import rohde.fitnesslogger.saving_classes.Workout;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

public class Result_Exercise_Result_Element_Workout extends Fragment {

    private static final String TIME = "time";
    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String WEIGHT = "weight";

    private long time;
    private String id;
    private String name;
    private float weight;
    private boolean rotated = false;



    ImageButton extend_btn;

    public Result_Exercise_Result_Element_Workout() {
        // Required empty public constructor
    }

    public static Result_Exercise_Result_Element_Workout newInstance(long time, String id, String name, float weight) {
        Result_Exercise_Result_Element_Workout fragment = new Result_Exercise_Result_Element_Workout();
        Bundle args = new Bundle();
        args.putLong(TIME, time);
        args.putString(ID, id);
        args.putString(NAME, name);
        args.putFloat(WEIGHT, weight);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            time = getArguments().getLong(TIME);
            id = getArguments().getString(ID);
            name = getArguments().getString(NAME);
            weight = getArguments().getFloat(WEIGHT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_result_exercise_result_element_workout, container, false);

        createDayTextView(view);

        TextView tv_time = view.findViewById(R.id.result_ex_result_element_workout_time);
        tv_time.setText(getTime(time));

        createSetsTextView(view);

        TextView tv_weight = view.findViewById(R.id.result_ex_result_element_workout_weight);
        tv_weight.setText(String.valueOf(weight));

        LinearLayout add_layout = view.findViewById(R.id.result_ex_result_element_workout_add_layout);

        extend_btn = view.findViewById(R.id.result_ex_result_element_workout_arrow);


        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RotateAnimation rotate = createRotateAnimation(add_layout);
                extend_btn.startAnimation(rotate);
            }
        };

        extend_btn.setOnClickListener(onClickListener);
        view.setOnClickListener(onClickListener);

        return view;
    }

    @NonNull
    private RotateAnimation createRotateAnimation(LinearLayout add_layout) {
        RotateAnimation rotate;
        if (rotated) {
            //HIDE
            rotate = new RotateAnimation(90, 0, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotated = false;
            hideAddLayout(add_layout);
        } else {
            //SHOW
            rotate = new RotateAnimation(0, 90, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            rotated = true;
            if (add_layout.getChildCount() == 0)
                addSetFragments(add_layout);
            else
                showAddLayout(add_layout);
            Savings.scrollItem = getView();
        }
        rotate.setDuration(100);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setFillAfter(true);
        return rotate;
    }

    private void hideAddLayout(LinearLayout add_layout) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.height = 0;
        add_layout.setLayoutParams(params);
        add_layout.setBackgroundColor(getResources().getColor(R.color.gray2));
    }

    private void showAddLayout(LinearLayout add_layout) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        add_layout.setLayoutParams(params);
        add_layout.setBackgroundColor(getResources().getColor(R.color.white));

    }

    private void addSetFragments(LinearLayout add_layout) {
        ArrayList<Result_Exercise_Result_Element_Sets> list = new ArrayList<>();
        for (Workout w : Savings.workouts) {
            if (w.getId().equals(id)) {
                for (Training_Set ts : w.getComplete_trainingSet()) {
                    if (ts.getExerciseName().equals(name)) {
                        for (Exercise e : ts.getExercises()) {
                            list.add(Result_Exercise_Result_Element_Sets.newInstance(e.getWeight(), e.getRepeats(), time));
                        }
                    }
                }
            }
        }

        //Add Fragment
        add_layout.setBackgroundColor(getResources().getColor(R.color.white));
        add_layout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        FragmentManager fragmentManager = getChildFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        for (Result_Exercise_Result_Element_Sets r : list) {
            fragmentTransaction.add(add_layout.getId(), r);

        }
        list.clear();
        fragmentTransaction.commit();
    }

    private void createSetsTextView(View view) {
        TextView tv_sets = view.findViewById(R.id.result_ex_result_element_workout_sets);
        int number_of_sets = 0;
        for (Workout w : Savings.workouts) {
            if (w.getId().equals(id)) {
                for (Training_Set ts : w.getComplete_trainingSet()) {
                    if (ts.getExerciseName().equals(name)) {
                        number_of_sets += ts.getExercises().size();
                    }
                }
            }
        }
        tv_sets.setText(String.valueOf(number_of_sets));
    }

    private void createDayTextView(View view) {
        TextView tv_day = view.findViewById(R.id.result_ex_result_element_workout_day);
        Date d = new Date(time);
        Calendar now = Calendar.getInstance();
        Calendar timeToCheck = Calendar.getInstance();
        timeToCheck.setTime(d);
        String day = "";

        if(now.get(Calendar.YEAR) == timeToCheck.get(Calendar.YEAR)) {
            if(now.get(Calendar.DAY_OF_YEAR) == timeToCheck.get(Calendar.DAY_OF_YEAR)) {
                day = "Today";
            } else if (now.get(Calendar.WEEK_OF_YEAR) - timeToCheck.get(Calendar.WEEK_OF_YEAR) <= 1){
                day = getDay(time);
            } else {
                day = getDate(time);
            }
        }
        tv_day.setText(day);
    }

    private String getTime(long timestamp) {
        Date date = new Date(timestamp);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("HH:mm");
        //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    private String getDay(long timestamp) {
        Date date = new Date(timestamp);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("E");
        //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    private String getDate(long timestamp) {
        Date date = new Date(timestamp);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("d MMM");
        //formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }
}
