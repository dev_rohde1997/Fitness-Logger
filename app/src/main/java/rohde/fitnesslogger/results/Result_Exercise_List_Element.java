package rohde.fitnesslogger.results;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;


public class Result_Exercise_List_Element extends Fragment {

    private static final String LABEL_TXT = "text";
    private static final String IMAGE_SRC = "src";

    private String label_text;
    private int image_src;

    public static Result_Exercise_List_Element newInstance(String text, int image) {
        Result_Exercise_List_Element fragment = new Result_Exercise_List_Element();
        Bundle args = new Bundle();
        args.putString(LABEL_TXT, text );
        args.putInt(IMAGE_SRC, image);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            label_text = getArguments().getString(LABEL_TXT);
            image_src = getArguments().getInt(IMAGE_SRC);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_exercise_list_element, container, false);

        TextView headline = view.findViewById(R.id.result_ex_element_label);
        ImageView image = view.findViewById(R.id.result_ex_element_image);
        LinearLayout linearLayout = view.findViewById(R.id.result_ex_element_layout);

        headline.setText(label_text);
        image.setImageResource(image_src);

        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Result_Activity.class);
                intent.putExtra(LABEL_TXT, label_text);
                startActivity(intent);
            }
        });

        return view;
    }


}
