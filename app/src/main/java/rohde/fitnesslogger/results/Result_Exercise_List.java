package rohde.fitnesslogger.results;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Objects;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.Category;
import rohde.fitnesslogger.saving_classes.Constants;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Training_Set;
import rohde.fitnesslogger.saving_classes.Workout;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;


public class Result_Exercise_List extends Fragment {

    ArrayList<Exerciser> used_exercisers;
    int used_ex_size = 0;
    boolean refresh = false;
    boolean displayExercises = true;


    LinearLayout add_layout_left;
    LinearLayout add_layout_right;
    ImageButton filter_btn;
    Spinner spinner;


    public Result_Exercise_List() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Init
        View view =  inflater.inflate(R.layout.fragment_result_exercise_list, container, false);
        add_layout_left = view.findViewById(R.id.result_list_add_layout_left);
        add_layout_right = view.findViewById(R.id.result_list_add_layout_right);

        Spinner spinner = view.findViewById(R.id.result_list_spinner);

        ArrayList<CharSequence> mList = new ArrayList<>();
        mList.add(getString(R.string.all_exercises));

        for (Category category : Savings.exercise_muscles_categories) {
            mList.add(category.getName());
        }

        ArrayAdapter<CharSequence> adapter= new ArrayAdapter<>(Objects.requireNonNull(getContext()), R.layout.spinner_item, mList);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Unfiltered data
                loadData();
                String type = "";
                if (position > 0) {
                    type = Savings.exercise_muscles_categories.get(position-1).getName();

                    ArrayList<Exerciser> mExerciser = new ArrayList<>();
                    for (Exerciser e : used_exercisers) {
                        if (e.getMuscleGroups() == null) continue;
                        for (String muscle : e.getMuscleGroups()) {
                            if (type.contains(muscle)) mExerciser.add(e);
                            break;
                        }
                    }
                    addExerciseElements(mExerciser);
                    displayExercises = true;
                } else {
                    if (displayExercises)
                        addExerciseElements(used_exercisers);
                        displayExercises = false;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }

        });


        return view;
    }

    private synchronized void addExerciseElements(ArrayList<Exerciser> exercisers) {

        add_layout_left.removeAllViews();
        add_layout_right.removeAllViews();

        //ADD Fragment
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();


        boolean left = true;
        LinearLayout add_layout;
        for (Exerciser e : exercisers) {
            if (left) {
                add_layout = add_layout_left;
                left = false;
            } else {
                add_layout = add_layout_right;
                left = true;
            }

            fragmentTransaction
                    .add(add_layout.getId(),
                            Result_Exercise_List_Element.newInstance(e.getName(), e.getImage()));
        }
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void loadData() {

        //Load App Exercises
        ArrayList<Exerciser> all_exercisers = new ArrayList(Savings.exercisers_global);
        used_exercisers = new ArrayList();

        for (Workout w : Savings.workouts) {
            for (Training_Set ts : w.getComplete_trainingSet()) {
                for (Exerciser e : all_exercisers) {
                    if (ts.getExerciseName().equals(e.getName())) {
                        used_exercisers.add(e);
                    }
                }
            }
        }

        used_exercisers = removeDuplicates(used_exercisers);
        Collections.sort(used_exercisers);
        if (used_ex_size != used_exercisers.size()) {
            refresh = true;
        }
        used_ex_size = used_exercisers.size();
    }

    private ArrayList<Exerciser> removeDuplicates(ArrayList<Exerciser> array) {
        LinkedHashSet<Exerciser> hashSet = new LinkedHashSet<>(array);
        return new ArrayList<>(hashSet);
    }

    @Override
    public void onResume() {
        loadData();
        if (refresh) {
            addExerciseElements(used_exercisers);
            refresh = false;
            displayExercises = false;

        }
        super.onResume();
    }
}
