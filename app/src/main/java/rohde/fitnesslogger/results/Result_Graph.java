package rohde.fitnesslogger.results;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.charts.CombinedChart.DrawOrder;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.TimeZone;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;
import rohde.fitnesslogger.saving_classes.DataPoint;
import rohde.fitnesslogger.saving_classes.DataPoint_WeightSum;
import rohde.fitnesslogger.saving_classes.Exercise;
import rohde.fitnesslogger.saving_classes.Exerciser;
import rohde.fitnesslogger.saving_classes.Training_Set;
import rohde.fitnesslogger.saving_classes.Workout;
import rohde.fitnesslogger.util.Month_Calculator;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;
import static rohde.fitnesslogger.saving_classes.Constants.PRIMARY_COLOR;
import static rohde.fitnesslogger.saving_classes.Constants.WEIGHT_VALUE_BIAS;


public class Result_Graph extends Fragment {

    private static final String EXERCISER = "ex";

    private String exerciser_name;
    private Fragment fragment = this;

    private int training_day_count = 0;
    private Date firstDate = null;
    private Date lastDate = null;
    private float maxWeight = 0;
    private float minWeight = Float.MAX_VALUE;


    //String[] x_axis_labels;
    ArrayList<DataPoint> dp_exercises;
    ArrayList<DataPoint_WeightSum> dataPoint_weightSums_exercises;
    ArrayList<DataPoint_WeightSum> dataPoint_weightSums_exercises_AVG;
    ScrollView scrollView;

    View scrollItem = null;



    public Result_Graph() {
        // Required empty public constructor
    }

    public static Result_Graph newInstance(String exerciser) {
        Result_Graph fragment = new Result_Graph();
        Bundle args = new Bundle();
        args.putString(EXERCISER, exerciser);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            exerciser_name = getArguments().getString(EXERCISER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_result_graph, container, false);

        //Set headline
        TextView headline = view.findViewById(R.id.result_graph_headline);
        headline.setText(exerciser_name);

        scrollView = view.findViewById(R.id.result_grpah_scrollview);

        //Image
        createImageView(view);

        //Back Button
        createBackButton(view);

        LineChart chart = createChart(view);
        LineDataSet lineDataSet = createData(createDataPoints());
        LineDataSet lineDataMax = createDataMaxLine(lineDataSet.getYMax(), lineDataSet.getXMax());
        LineDataSet lineDataMaxValue = createDataMaxValue();

        createResultDetailList(view);
        LineDataSet lineDataSet_avg = createAVGData(dataPoint_weightSums_exercises_AVG);
        createMinDateLabel(view);


        chart.setData(new LineData(lineDataSet, lineDataSet_avg, lineDataMax, lineDataMaxValue));
        chart.getLegend().setEnabled(false);
        configureAxis(chart);
        chart.invalidate(); // refresh




        @SuppressLint("HandlerLeak") final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {

                scrollView.post(() -> ObjectAnimator.ofInt(scrollView, "scrollY",
                        scrollItem.getBottom() + 150).setDuration(500).start());

            }
        };

        Thread checkScroll = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try {
                        Thread.sleep(25);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (Savings.scrollItem != null) {
                        scrollItem = Savings.scrollItem;
                        Savings.scrollItem = null;
                        handler.sendEmptyMessage(0);
                    }
                }
            }
        });
        checkScroll.start();


        return view;
    }

    private void createMinDateLabel(View view) {
        TextView tv_min_date_label = view.findViewById(R.id.result_graph_first_date);
        tv_min_date_label.setText(convertTimestampToString(firstDate.getTime()));
    }

    private void createResultDetailList(View view) {
        Collections.reverse(dataPoint_weightSums_exercises);
        if (dataPoint_weightSums_exercises.size() != 0) {
            addWeekLabelElement(view, dataPoint_weightSums_exercises.get(0).getLongTimestamp(),
                    estimateWorkoutCounter(dataPoint_weightSums_exercises.get(0)));
            addResultsWorkoutElement(view,
                    dataPoint_weightSums_exercises.get(0).getLongTimestamp(),
                    dataPoint_weightSums_exercises.get(0).getWorkoutID(),
                    dataPoint_weightSums_exercises.get(0).getWeight_sum());
        }


        for (int i = 1; i < dataPoint_weightSums_exercises.size(); i++) {
            DataPoint_WeightSum dp = dataPoint_weightSums_exercises.get(i);

            //CALENDER
            Calendar this_cal = Calendar.getInstance();
            this_cal.setTime(new Date(dp.getLongTimestamp()));

            Calendar last_cal = Calendar.getInstance();
            last_cal.setTime(new Date(dataPoint_weightSums_exercises.get(i - 1).getLongTimestamp()));

            if (this_cal.get(Calendar.WEEK_OF_YEAR) != last_cal.get(Calendar.WEEK_OF_YEAR)) {
                addWeekLabelElement(view, dp.getLongTimestamp(), estimateWorkoutCounter(dp));

            }

            addResultsWorkoutElement(view,
                    dp.getLongTimestamp(),
                    dp.getWorkoutID(),
                    dp.getWeight_sum());
        }
        Collections.reverse(dataPoint_weightSums_exercises);


        dataPoint_weightSums_exercises_AVG = new ArrayList<>();
        dataPoint_weightSums_exercises_AVG.add(dataPoint_weightSums_exercises.get(0));

        float sum = dataPoint_weightSums_exercises.get(0).getWeight_sum();

        for (int i = 1; i < dataPoint_weightSums_exercises.size(); i++) {
            DataPoint_WeightSum dp = dataPoint_weightSums_exercises.get(i);

            //AVG
            //Just the last five values are used for average

            float mSum = 0;
            int j = i;
            int values_considered = 2;
            int value_count = 0;

            while (j >= 0 & j > i - values_considered) {
                mSum += dataPoint_weightSums_exercises.get(j).getWeight_sum();
                j--;
                value_count++;
            }

            float weight_sum = mSum / (value_count);
            dp.setWeight_sum(weight_sum);
            dataPoint_weightSums_exercises_AVG.add(dp);
        }


    }

    private int estimateWorkoutCounter(DataPoint_WeightSum dp) {
        //Estimate workout counter
        int workout_counter = 0;

        //First day
        Date first_weekday = Month_Calculator.getFirstDayOfWeek(new Date(dp.getLongTimestamp()));
        Calendar first = Calendar.getInstance();
        first.setTime(first_weekday);
        int week_begin = first.get(Calendar.DAY_OF_YEAR);

        //Last day
        Date last_weekday = Month_Calculator.getLastDayOfWeek(new Date(dp.getLongTimestamp()));
        Calendar last = Calendar.getInstance();
        last.setTime(last_weekday);
        int week_end = last.get(Calendar.DAY_OF_YEAR);

        Calendar cal = Calendar.getInstance();
        for (DataPoint_WeightSum dataPointWeightSum : dataPoint_weightSums_exercises) {
            cal.setTime(new Date(dataPointWeightSum.getLongTimestamp()));

            int day_of_year = cal.get(Calendar.DAY_OF_YEAR);
            if (day_of_year >= week_begin && day_of_year <= week_end) {
                workout_counter++;
            }

        }
        return workout_counter;
    }

    private LineChart createChart(View view) {
        LineChart chart = view.findViewById(R.id.result_graph_chart);
        chart.setNoDataText("No data for the moment");
        chart.setHighlightPerTapEnabled(false);
        chart.setHorizontalScrollBarEnabled(false);
        chart.setVerticalScrollBarEnabled(false);
        chart.setScaleEnabled(false);
        chart.setDescription(null);
        chart.setTouchEnabled(false);
        return chart;
    }

    private void configureAxis(LineChart chart) {
        //X-axis
        XAxis xa = chart.getXAxis();
        long dateBias = (lastDate.getTime() - firstDate.getTime()) / 8;
        //xa.setAxisMinimum(firstDate.getTime() - dateBias);
        //xa.setAxisMaximum(lastDate.getTime() + dateBias);

        xa.setPosition(XAxis.XAxisPosition.BOTTOM);
        xa.setDrawGridLines(false);
        xa.setDrawLabels(false);
        xa.setEnabled(true);
        xa.setTextSize(getResources().getDimension(R.dimen.text_size_default));
        xa.setTextColor(Color.WHITE);

        xa.setAxisMinimum(1);
        xa.setAxisMaximum(dataPoint_weightSums_exercises.size());


        //Y-axis
        YAxis ya = chart.getAxisLeft();
        ya.setTextColor(Color.WHITE);
        ya.setTextSize(15);
        if (minWeight - WEIGHT_VALUE_BIAS < 0) {
            ya.setAxisMinimum(0);
        } else {
            ya.setAxisMinimum(minWeight - WEIGHT_VALUE_BIAS);
        }
        ya.setAxisMaximum(maxWeight + WEIGHT_VALUE_BIAS);
        ya.setDrawGridLines(true);
        ya.setGridColor(getResources().getColor(R.color.whiteTrans4));
        ya.setZeroLineColor(getResources().getColor(R.color.whiteTrans4));

        //Achse??
        YAxis y12 = chart.getAxisRight();
        y12.setEnabled(false);
    }

    private LineDataSet createData(ArrayList<DataPoint_WeightSum> dataObjects) {
        List<Entry> entries = new ArrayList<>();
        int count = 0;
        for (DataPoint_WeightSum data : dataObjects) {
            count++;
            // turn your data into Entry objects
            //entries.add(new Entry(data.getTimestamp(), data.getWeight_sum()));        }
            entries.add(new Entry(count, data.getWeight_sum()));
        }

        LineDataSet dataSet = new LineDataSet(entries, "Weight"); // add entries to dataset
        dataSet.setColor(Color.WHITE);
        dataSet.setCircleColor(Color.WHITE);
        dataSet.setDrawFilled(true);
        dataSet.setFillColor(Color.WHITE);
        dataSet.setDrawCircleHole(false);
        dataSet.setDrawCircles(false);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setCircleRadius(2);
        dataSet.setLineWidth(1.5f);
        dataSet.setDrawValues(false);

        ArrayList<String> temp_label_list = new ArrayList<>();
        for (DataPoint_WeightSum d : dataObjects) {
            //Convert timestamp to String
            float timestamp = d.getTimestamp();
            String dateFormatted = convertTimestampToString((long) timestamp);
            temp_label_list.add(dateFormatted);
        }

      /*  temp_label_list = removeDuplicates(temp_label_list);

        x_axis_labels = new String[temp_label_list.size()];

        temp_label_list.toArray(x_axis_labels);
*/

        return dataSet;

    }

    private LineDataSet createAVGData(ArrayList<DataPoint_WeightSum> dataObjects) {
        List<Entry> entries = new ArrayList<>();
        int count = 0;
        for (DataPoint_WeightSum data : dataObjects) {
            // turn your data into Entry objects
            count++;
            //entries.add(new Entry(data.getTimestamp(), data.getWeight_sum()));
            entries.add(new Entry(count, data.getWeight_sum()));
        }

        LineDataSet dataSet = new LineDataSet(entries, "AVG"); // add entries to dataset

        dataSet.setColor(Color.RED);
        dataSet.setDrawCircleHole(false);
        dataSet.setDrawCircles(false);
        dataSet.setCircleColor(Color.RED);
        dataSet.setCircleRadius(1.5f);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSet.setLineWidth(3);
        dataSet.setDrawValues(false);
        return dataSet;

    }

    private LineDataSet createDataMaxLine(float max, float length) {
        List<Entry> entries = new ArrayList<>();

        entries.add(new Entry(0, max));
        entries.add(new Entry(length, max));

        LineDataSet dataSet = new LineDataSet(entries, "Max"); // add entries to dataset
        dataSet.setColor(Color.WHITE);
        dataSet.setDrawCircleHole(false);
        dataSet.setDrawCircles(false);
        dataSet.enableDashedLine(10, 5, 2);
        dataSet.setLineWidth(1f);
        dataSet.setDrawValues(false);

        return dataSet;

    }

    private LineDataSet createDataMaxValue() {

        float max_x = 0;
        float max_y = 0;
        for (int i = 0; i < dataPoint_weightSums_exercises.size(); i++) {
            if (dataPoint_weightSums_exercises.get(i).getWeight_sum() > max_y) {
                max_x = i+1;
                max_y = dataPoint_weightSums_exercises.get(i).getWeight_sum();
            }
        }

        List<Entry> entries = new ArrayList<>();

        entries.add(new Entry(max_x, max_y));

        LineDataSet dataSet = new LineDataSet(entries, "Max Value"); // add entries to dataset
        dataSet.setColor(Color.WHITE);
        dataSet.setDrawCircleHole(false);
        dataSet.setCircleColor(Color.WHITE);
        dataSet.setFormSize(5);
        dataSet.setValueTextSize(10);
        dataSet.setValueTextColor(Color.WHITE);
        dataSet.setDrawCircles(true);
        dataSet.setDrawValues(true);

        return dataSet;

    }

    private String convertTimestampToString(long timestamp) {
        Date date = new Date(timestamp);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("dd/MM");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }

    private void createImageView(View view) {
        ImageView image = view.findViewById(R.id.result_graph_image);
        int image_id = -1;
        ArrayList<Exerciser> exercisers = new ArrayList<>(Savings.exercisers_global);
        for (Exerciser e : exercisers) {
            if (e.getName().equals(exerciser_name)) {
                image_id = e.getImage();
            }
        }
        image.setImageResource(image_id);
    }

    private void createBackButton(View view) {
       // ImageButton back_btn = view.findViewById(R.id.result_graph_back_btn);
      /*  back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                assert fragmentManager != null;
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.detach(fragment).commit();
            }
        });*/
    }

    private ArrayList<DataPoint_WeightSum> createDataPoints() {
        ArrayList<DataPoint> data = new ArrayList<>();
        for (Workout w : Savings.workouts) {
            long date = evaluateDate(w);
            for (Training_Set set : w.getComplete_trainingSet()) {
                for (Exercise e : set.getExercises()) {
                    if (e.getName().equals(exerciser_name)) {
                        data.add(new DataPoint(date, new Exercise(exerciser_name, e.getRepeats(), e.getWeight()), w.getId()));
                    }
                }
            }
        }

        Collections.sort(data);
        dp_exercises = new ArrayList<>(data);
        Collections.reverse(dp_exercises);

        ArrayList<DataPoint_WeightSum> dataPointWeightSums = new ArrayList<>();
        float weight_sum = 0;
        for (int i = 0; i < data.size(); i++) {
            DataPoint dp = data.get(i);

            weight_sum += dp.getExercise().getRepeats() * dp.getExercise().getWeight();

            if (i + 1 == data.size() || dp.getTimestamp() != data.get(i+1).getTimestamp()) {
                dataPointWeightSums.add(new DataPoint_WeightSum(dp.getTimestamp(), weight_sum, dp.getWorkoutID()));
                weight_sum = 0;
            }
        }

        for (DataPoint_WeightSum dataPoint_weightSum : dataPointWeightSums) {
            float weight_sum_check = dataPoint_weightSum.getWeight_sum();
            if (weight_sum_check > maxWeight) maxWeight = weight_sum_check;
            if (weight_sum_check < minWeight) minWeight = weight_sum_check;
        }

        checkDates(dataPointWeightSums);

        dataPoint_weightSums_exercises = new ArrayList<>(dataPointWeightSums);
        return dataPointWeightSums;
    }

    private void checkDates(ArrayList<DataPoint_WeightSum> dataPointWeightSums) {
        //Check first date
        float min_date = -1;
        for (DataPoint_WeightSum d : dataPointWeightSums) {
            if (min_date == -1 ) {
                min_date = d.getTimestamp();
            } else if (min_date > d.getTimestamp()) {
                min_date = d.getTimestamp();
            }
        }

        if (min_date > firstDate.getTime()) firstDate = new Date((long) min_date);

        //Check late date
        float max_date = -1;
        for (DataPoint_WeightSum d : dataPointWeightSums) {
            if (max_date == -1 ) {
                max_date = d.getTimestamp();
            } else if (max_date < d.getTimestamp()) {
                max_date = d.getTimestamp();
            }
        }
        if (max_date < lastDate.getTime()) lastDate = new Date((long) max_date);
    }

    private long evaluateDate(Workout w) {
        Date date = w.getDate();
        if (firstDate == null ) {
            firstDate = date;
            lastDate = date;
        }
        if (date.before(firstDate)) firstDate = date;
        if (date.after(lastDate)) lastDate = date;
        return date.getTime();
    }

    private ArrayList removeDuplicates(ArrayList array) {
        LinkedHashSet<Exerciser> hashSet = new LinkedHashSet<>(array);
        return new ArrayList<>(hashSet);
    }



    private void addWeekLabelElement(View view, long time, int workouts_number) {

        LinearLayout linearLayout = view.findViewById(R.id.result_graph_results_view);
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(linearLayout.getId(),
                        Result_Exercise_Result_Week_Element.newInstance(time, workouts_number))
                .commit();
    }

    private void addResultsWorkoutElement(View view, long time, String id, float weight_sum) {
        LinearLayout linearLayout = view.findViewById(R.id.result_graph_results_view);
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction
                .add(linearLayout.getId(),
                        Result_Exercise_Result_Element_Workout
                                .newInstance(time, id, exerciser_name, weight_sum))
                .commit();
    }
}
