package rohde.fitnesslogger.results;

import android.annotation.SuppressLint;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.support.v4.app.Fragment;
import rohde.fitnesslogger.R;
import rohde.fitnesslogger.util.Month_Calculator;


public class Result_Exercise_Result_Week_Element extends Fragment {


    private static final String TIME = "time";
    private static final String WORKOUTS = "workouts";

    private long time;
    private int workouts;

    public Result_Exercise_Result_Week_Element() {
        // Required empty public constructor
    }

    public static Result_Exercise_Result_Week_Element newInstance(long time, int workouts) {
        Result_Exercise_Result_Week_Element fragment = new Result_Exercise_Result_Week_Element();
        Bundle args = new Bundle();
        args.putLong(TIME, time);
        args.putInt(WORKOUTS, workouts);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            time = getArguments().getLong(TIME);
            workouts = getArguments().getInt(WORKOUTS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_result_exercise_result_week_element, container, false);

        TextView tv_week_label = view.findViewById(R.id.result_ex_result_week_week);

        String week_text = "";

        //Init calender
        Calendar calendar = Calendar.getInstance(new Locale("de","DE"));
        Date date = new Date(time);
        calendar.setTime(date);


        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.WEEK_OF_YEAR) == calendar.get(Calendar.WEEK_OF_YEAR)) {
            week_text = "This Week";
        } else {

            Date weekStart = Month_Calculator.getFirstDayOfWeek(date);
            Calendar weekStartCal = Calendar.getInstance();
            weekStartCal.setTime(weekStart);

            Date weekEnd = Month_Calculator.getLastDayOfWeek(date);
            Calendar weekEndCal = Calendar.getInstance();
            weekEndCal.setTime(weekEnd);

             String first_date_str = getMonth(weekStart.getTime()) + " " + weekStartCal.get(Calendar.DAY_OF_MONTH);
             String last_date_str =  getMonth(weekEnd.getTime()) + " " + weekEndCal.get(Calendar.DAY_OF_MONTH);

             week_text = first_date_str + " - " + last_date_str;
         }

        tv_week_label.setText(week_text);

        TextView tv_week_workouts = view.findViewById(R.id.result_ex_result_week_workout_number);
        String workout_text = "";

        if (workouts == 1) {
            workout_text = workouts + " " + getString(R.string.workout);
        } else {
            workout_text = workouts + " " + getString(R.string.workouts);

        }
        tv_week_workouts.setText(workout_text);

        return view;
    }

    private String getMonth(long timestamp) {
        Date date = new Date(timestamp);
        @SuppressLint("SimpleDateFormat") DateFormat formatter = new SimpleDateFormat("MMM");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        return formatter.format(date);
    }
}
