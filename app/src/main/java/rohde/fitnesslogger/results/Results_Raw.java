package rohde.fitnesslogger.results;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;

public class Results_Raw extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_results_raw, container, false);
        TextView tv_raw = view.findViewById(R.id.results_raw_tv);
        tv_raw.setText(Savings.workouts.toString());
        return view;
    }
}
