package rohde.fitnesslogger.saving_classes;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;

import rohde.fitnesslogger.main.Savings;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

public class Workout_plan implements Comparable<Workout_plan>{
    private String name;
    private long id;
    private ArrayList<Exerciser> exerciser;

    public Workout_plan(long id, String name, ArrayList<Exerciser> exerciser) {
        this.id = id;
        this.name = name;
        this.exerciser = exerciser;
    }

    public Workout_plan(ArrayList<Exerciser> exerciser) {
        this.exerciser = exerciser;
    }

    public Workout_plan(long id, String compactDataString) {
        this.id = id;
        String[] compactDataStringSplit = compactDataString.split("\\|");
        this.name = compactDataStringSplit[0];
        this.exerciser = new ArrayList<>();

        for (int i = 1; i < compactDataStringSplit.length; i++) {
            // Find exerciser by a given name
            for (Exerciser e : Savings.exercisers_global) {
                if (e.getName().equals(compactDataStringSplit[i])) {
                    exerciser.add(e);
                    break;
                }
            }
        }
    }

    @Override
    public int compareTo(Workout_plan other) {
        if (other == null) throw new IllegalArgumentException();

        int i;
        for (i = 0; i < getName().length() & i < other.getName().length(); i++) {
            if (getName().charAt(i) != other.getName().charAt(i))
                return getName().charAt(i) - other.getName().charAt(i);
        }
         return getName().length() - other.getName().length();
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<Exerciser> getExerciser() {
        return exerciser;
    }

    public void setExerciser(ArrayList<Exerciser> exerciser) {
        this.exerciser = exerciser;
    }

    public int getMuscleTypeCount(String type) {
        int count = 0;
        a: for (Exerciser e : exerciser) {
            if (e.getMuscleGroups() == null) continue;
            for (String s : e.getMuscleGroups()) {
                if (type.contains(s)) {
                    count++;
                    continue a;
                }
            }
        }
        return count;
    }

    public int getTypeCount(String type) {
        int count = 0;
        for (Exerciser e : exerciser) {
            if (e.getType() == null) continue;
            if (type.equals(e.getType())) {
                count++;
            }

        }
        return count;
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        out.append(name).append("|");

        for (Exerciser e : exerciser) {
            out.append(e.getName()).append("|");
            //out.append(e.getDefaultWeight()).append("|");
        }

        return out.toString();
    }
}
