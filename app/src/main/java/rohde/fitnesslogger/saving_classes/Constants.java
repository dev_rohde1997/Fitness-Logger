package rohde.fitnesslogger.saving_classes;

public class Constants {
    public static final String LOG_TAG = " | Fitness_Logger";
    public static final String SAVE_TAG_WORKOUT_PLAN = "Workout_Plan";
    public static final String SAVE_TAG_TRAINING = "Training";
    public static final String SAVE_TAG_SELF_CREATED_EXERCISER = "SelfCreated";
    public static final String SAVE_TAG_EXERCISER_DEFAULT_WEIGHT = "Exerciser Default Weight";
    public static final String SAVE_TAG_TEMP_WORKOUT_DATA = "Temp Workout Data";
    public static final String TAG_WORKOUT_NOT_SAVED= "Workout not saved";
    public static final String TAG_WORKOUT_NOT_SAVED_ID= "Workout ID not saved";
    public static final String TAG_EXERCISE_NOT_SAVED= "Exercise not saved";
    public static final double MAX_WEIGHT = 250.0;
    public static final int MAX_REPEATS = 30;
    public static final int PRIMARY_COLOR = 0x009977;
    static final int DATE_YEAR_SHIFT = 1900;
    public static final int WEIGHT_VALUE_BIAS = 250;
    public static final int BUTTONS_HOLD_TIME = 200;
}
