package rohde.fitnesslogger.saving_classes;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

public class Muscles {
    //Name, resId
    public static HashMap<String, Integer> front_muscles_with_resId = new HashMap<>();
    public static HashMap<String, Integer> back_muscles_with_resId = new HashMap<>();

    public static void removeZeroIds() {
        //Remove zero values of front muscles
        ArrayList<String> removableKeys = new ArrayList<>();
        for (String key : front_muscles_with_resId.keySet()) {
            if (front_muscles_with_resId.get(key) == 0) {
                removableKeys.add(key);
            }
        }

        for (String key : removableKeys) {
            front_muscles_with_resId.remove(key);
        }

        //Remove zero values of back muscles
        ArrayList<String> removableKeys2 = new ArrayList<>();
        for (String key : back_muscles_with_resId.keySet()) {
            if (back_muscles_with_resId.get(key) == 0) {
                removableKeys2.add(key);
            }
        }

        for (String key : removableKeys2) {
            back_muscles_with_resId.remove(key);
        }
    }
}
