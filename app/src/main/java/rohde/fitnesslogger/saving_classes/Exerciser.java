package rohde.fitnesslogger.saving_classes;


import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import rohde.fitnesslogger.R;
import rohde.fitnesslogger.main.Savings;

import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

//TODO: Is without weights, just repeats

public class Exerciser implements Comparable<Exerciser>{
    private String name;
    private int image;
    private double default_weight;
    private Double[] weight_steps;
    private String[] muscle_groups;
    private String type;


    public Exerciser(String name, int image, String[] muscle_groups, Double[] weight_steps, double default_weight, String type) {
        this.name = name;
        this.image = image;
        this.weight_steps = weight_steps;
        this.default_weight = default_weight;
        this.muscle_groups = muscle_groups;
        this.type = type;
    }

    public Exerciser(String name, int image, double default_weight, String type, String[] muscle_groups) {
        this.name = name;
        this.image = image;
        this.default_weight = default_weight;
        this.type = type;
        this.muscle_groups = muscle_groups;
    }

    public Exerciser(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public Exerciser(String name) {
        this.name = name;
        this.image = R.drawable.questionmark;
    }

    public Exerciser(String name, String data) {
        this.name = name;
        decodeSavedExerciserList(data);
    }

    public String[] getMuscleGroups() {
        return muscle_groups;
    }

    public Double[] getWeightSteps() {
        return weight_steps;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setMuscle_groups(String[] muscle_groups) {
        this.muscle_groups = muscle_groups;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDefaultWeight() {
        return default_weight;
    }

    public void setDefaultWeight(double default_weight) {
        this.default_weight = default_weight;
    }

    @NonNull
    @Override
    public String toString() {
        return name + ";" + default_weight + ";" + type + ";" + Arrays.toString(muscle_groups);
    }

    @Override
    public int compareTo(Exerciser other) {
        if (other == null) throw new IllegalArgumentException();

        int i;
        for (i = 0; i < getName().length() & i < other.getName().length(); i++) {
            if (getName().charAt(i) != other.getName().charAt(i))
                return getName().charAt(i) - other.getName().charAt(i);
        }
        return getName().length() - other.getName().length();
    }

    static ArrayList<Exerciser> decodeSavedExerciserList(String compactData) throws IndexOutOfBoundsException, NumberFormatException{
        double default_weight;
        String my_name;
        String type;
        ArrayList<Exerciser> exercisers = new ArrayList<>();
        String[] muscles = null;

        //Remove square brackets
        compactData = compactData.substring(1, compactData.length()-1);


        //Splitting
        String[] exerciser_strings = compactData.split("], ");
        for (String ex_s : exerciser_strings) {
            ex_s += "]";
            String[] exerciser_single_data = ex_s.split(";");
            my_name = exerciser_single_data[0];
            default_weight = Double.parseDouble(exerciser_single_data[1]);
            int image = findImage(my_name);
            type = exerciser_single_data[2];
            muscles = exerciser_single_data[3]
                    .substring(1, exerciser_single_data[3].length()-1)
                    .replace(" ", "")
                    .split(",");

            Exerciser exerciser = new Exerciser(my_name, image, default_weight, type, muscles);


            exercisers.add(exerciser);
        }
        if (exercisers.size() == 0) return null;
        return exercisers;
    }

    public static Exerciser decodeSavedSingleExerciser(String compactData) throws IndexOutOfBoundsException, NumberFormatException{
        double default_weight;
        String my_name;
        String type;
        String[] muscles = null;


        //Splitting
        String[] exerciser_single_data = compactData.split(";");
        my_name = exerciser_single_data[0];
        default_weight = Double.parseDouble(exerciser_single_data[1]);
        int image = findImage(my_name);
        type = exerciser_single_data[2];
        muscles = exerciser_single_data[3]
                .substring(1, exerciser_single_data[3].length()-1)
                .replace(" ", "")
                .split(",");

        return new Exerciser(my_name, image, default_weight, type, muscles);

    }


    private static int findImage(String name) {
        for (Exerciser e: Savings.exercisers_global) {
            if (e.getName().equals(name)) return e.getImage();
        }
        return 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exerciser exerciser = (Exerciser) o;

        return name.equals(exerciser.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }
}
