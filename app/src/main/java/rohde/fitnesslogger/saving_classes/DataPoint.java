package rohde.fitnesslogger.saving_classes;

import java.util.Date;

public class DataPoint implements Comparable<DataPoint> {
    private float timestamp;
    private Exercise exercise;
    private String workout_id;


    public DataPoint(float timestamp, Exercise exercise, String workout_id) {
        this.timestamp = timestamp;
        this.exercise = exercise;
        this.workout_id = workout_id;
    }

    public String getWorkoutID() {
        return workout_id;
    }

    public float getTimestamp() {
        return timestamp;
    }

    public long getLongTimestamp() {
        return (long) timestamp;
    }

    public Exercise getExercise() {
        return exercise;
    }

    @Override
    public int compareTo(DataPoint o) {
        return (int) (timestamp - o.getTimestamp());
    }
}
