package rohde.fitnesslogger.saving_classes;

import android.util.Log;

public class Exercise {

    private String name;
    private int repeats;
    private double weight;

    public Exercise(String name, int repeats, double weight) {
        this.name = name;
        this.repeats = repeats;
        this.weight = weight;
    }

    public Exercise(String[] data) {
        if (data.length != 3) {
            Log.e(getClass().getSimpleName() + Constants.LOG_TAG, String.format("Wrong Length (%d) during creating an exercise", data.length));
            for(String s : data) Log.e(getClass().getSimpleName() + Constants.LOG_TAG, s);
            return;
        }
        //Log.i(Constants.LOG_TAG, "Creating exercise: " + data[0] + ", " + data[1] + ", " + data[2]);
        this.name = data[0];
        this.repeats = Integer.parseInt(data[1]);
        this.weight = Double.parseDouble(data[2]);
        //Log.i(Constants.LOG_TAG, "Exercises Created");

    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRepeats() {
        return repeats;
    }

    public void setRepeats(int repeats) {
        this.repeats = repeats;
    }

    public double getWeight() {
        return weight;
    }

    public float getFloatWeight() {
        return (float) weight;
    }
    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return name + "|" + repeats + "|" + weight;
    }
}
