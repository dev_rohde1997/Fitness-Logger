package rohde.fitnesslogger.saving_classes;


import java.util.ArrayList;
import java.util.Calendar;

public class Training_Set {
    private long id;
    private ArrayList<Exercise> exercises;


    public Training_Set(ArrayList<Exercise> exercises) {
        this.id = Calendar.getInstance().getTimeInMillis();
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        this.exercises = exercises;
    }

    public Training_Set(Training_Set training_set) {
        this.id = training_set.getId();
        this.exercises = training_set.getExercises();
    }

    public ArrayList<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(ArrayList<Exercise> exercises) {
        this.exercises = exercises;
    }

    public void addExercise(Exercise e) {
        getExercises().add(e);
    }

    @Override
    public String toString() {
        return exercises.toString();
    }

    public int getSets() {
        return getExercises().size();
    }

    public String getExerciseName() {
        if (getExercises() == null || getExercises().size() == 0)
            return null;
        else
            return getExercises().get(0).getName();
    }


    public static ArrayList<Exercise> getReverseFormat(String data) {
        StringBuilder rawData = new StringBuilder();
        for (int i = 0; i < data.length(); i++) {
            char c = data.charAt(i);
            if (c == '[' || c == ']')
                continue;
            rawData.append(data.charAt(i));
        }

        String[] trainings = rawData.toString().split(", ");
        ArrayList<Exercise> exercises = new ArrayList<>();
        for (String s : trainings) {
            String[] exercise_stats = s.split("\\|");
            exercises.add(new Exercise(exercise_stats));
        }

        return exercises;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object obj) throws ClassCastException{
        Training_Set training_set = (Training_Set) obj;
        return id == training_set.id;
    }
}
