package rohde.fitnesslogger.saving_classes;

import java.util.ArrayList;
import java.util.HashMap;

public class Category {

    private String name;
    private int resourceImageID;
    private HashMap<String, Exerciser> exercisers;

    public Category(String name, int resourceImageID, HashMap<String, Exerciser> exercisers) {
        this.name = name;
        this.resourceImageID = resourceImageID;
        this.exercisers = exercisers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResourceImageID() {
        return resourceImageID;
    }

    public void setResourceImageID(int resourceImageID) {
        this.resourceImageID = resourceImageID;
    }

    public HashMap<String, Exerciser> getExercisers() {
        return exercisers;
    }

    public void setExercisers(HashMap<String, Exerciser> exercisers) {
        this.exercisers = exercisers;
    }
}
