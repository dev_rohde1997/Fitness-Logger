package rohde.fitnesslogger.saving_classes;

import android.support.annotation.NonNull;

public class DataPoint_WeightSum implements Comparable<DataPoint_WeightSum>{
    private float timestamp;
    private float weight_sum;
    private String workout_id;

    public DataPoint_WeightSum(float timestamp, float weight_sum, String workout_id) {
        this.timestamp = timestamp;
        this.weight_sum = weight_sum;
        this.workout_id = workout_id;
    }

    public String getWorkoutID() {
        return workout_id;
    }

    public float getTimestamp() {
        return timestamp;
    }

    public float getWeight_sum() {
        return weight_sum;
    }

    public void setWeight_sum(float weight_sum) {
        this.weight_sum = weight_sum;
    }

    @NonNull
    @Override
    public String toString() {
        return "" + timestamp + "/" + weight_sum;
    }

    @Override
    public int compareTo(DataPoint_WeightSum o) {
        return (int) (timestamp - o.getTimestamp());
    }

    public long getLongTimestamp() {
        return (long) timestamp;
    }
}
