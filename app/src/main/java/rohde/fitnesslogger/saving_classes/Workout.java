package rohde.fitnesslogger.saving_classes;

import android.util.Log;

import java.util.ArrayList;
import java.util.Date;

import rohde.fitnesslogger.util.Month_Calculator;

import static rohde.fitnesslogger.saving_classes.Constants.DATE_YEAR_SHIFT;
import static rohde.fitnesslogger.saving_classes.Constants.LOG_TAG;

public class Workout implements Comparable<Workout>{
    private String id;
    private ArrayList<Training_Set> complete_trainingSet = new ArrayList<>();

    public Workout(String id, ArrayList<Training_Set> complete_trainingSet) {
        this.id = id;
        this.complete_trainingSet = complete_trainingSet;
    }

    public Workout(String id, String compactDataString) {
        this.id = id;
        ArrayList<Exercise> exercises = Training_Set.getReverseFormat(compactDataString);

        ArrayList<Exercise> temp = new ArrayList<>();
        Exercise last = null;
        for (Exercise e : exercises) {
            if (last == null) {
                last = e;
                temp.add(e);
            } else if (last.getName().equals(e.getName())) {
                temp.add(e);
            } else {
                complete_trainingSet.add(new Training_Set(temp));
                temp = new ArrayList<>();
                temp.add(e);
            }
        }
        //Addling last element
        //Log.i(Constants.LOG_TAG, "Save: " + temp);
        complete_trainingSet.add(new Training_Set(temp));

        //Log.i(Constants.LOG_TAG, "Workout: " + complete_trainingSet);
       // Log.i(Constants.LOG_TAG, "Workout Created");
    }

    public ArrayList<Training_Set> getComplete_trainingSet() {
        return complete_trainingSet;
    }

    public void setComplete_trainingSet(ArrayList<Training_Set> complete_trainingSet) {
        this.complete_trainingSet = complete_trainingSet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return complete_trainingSet.toString();
    }

    @Override
    public int compareTo(Workout o) {
        long ret = id_to_long(getId()) - id_to_long(o.getId());
        if (ret > Integer.MAX_VALUE) return  Integer.MAX_VALUE;
        if (ret < Integer.MIN_VALUE) return  Integer.MIN_VALUE;
        return (int) ret;

    }

    private long id_to_long(String id) {
        String[]id_parts = id.split(" ");
        String[] time = id_parts[3].split(":");
        return Long.parseLong(id_parts[5] + Month_Calculator.month_to_int(id_parts[1]) + id_parts[2] + time[0] + time[1] + time[2]);
    }

    //Format: Sat Jun 08 12:59:11 GMT+00:00 2019
    public Date getDate() throws IndexOutOfBoundsException, NumberFormatException {
        String[] splitID = id.split(" ");
        int year = Integer.parseInt(splitID[5]);
        int month = Month_Calculator.month_to_int(splitID[1]);
        int day = Integer.parseInt(splitID[2]);

        String[] time_split = splitID[3].split(":");
        int hrs = Integer.parseInt(time_split[0]);
        int min = Integer.parseInt(time_split[1]);
        return new Date(year - DATE_YEAR_SHIFT, month, day, hrs, min);
    }



}
