package rohde.fitnesslogger;

import org.junit.Test;

import rohde.fitnesslogger.util.GenerateDoubleArrayWeightSteps;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UnitTest {


    @Test
    public void checkArrayGenerate() {
        String list = "0,4,9,13,45,67,90";
        Double[] d = GenerateDoubleArrayWeightSteps.generateArray(list.split(","));
        Double[] d2 = new Double[]{0d, 4d, 9d, 13d, 45d, 67d, 90d};
        assertArrayEquals(d, d2);
    }
}